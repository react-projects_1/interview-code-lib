# 1 安装全局的依赖

pnpm install lodash -w
-w 就是 workspace 的意思
移除全局依赖：pnpm remove lodash

# 2 安装依赖到独立的子模块下

pnpm add axios --filter hello-demo
移除依赖：pnpm remove axios --filter hello-demo

# 3 在每个子模块都安装依赖

pnpm install axios -r

# 4 子模块之间依赖管理

pnpm add demo --filter react-dnd-source-impl
就是将依赖 demo 安装到了子项目 react-dnd-source-impl 下了

# 5 命令执行

pnpm test：表示执行根目录下的 package.json 下的 test 命令
pnpm test --filter hello-world：表示执行的是 hello-world 下的 package.json test 命令
pnpm test -r：表示递归自行所有子项目里的 test 命令
