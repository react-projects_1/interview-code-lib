// 非原地快排，空间时间复杂度都是nlogn
function quickSort(arr) {
  if (arr.length <= 1) return arr
  const index = Math.floor(Math.random() * arr.length)
  const left = []
  const right = []
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] < arr[index] && i !== index) {
      left.push(arr[i])
    }
    if (arr[i] >= arr[index] && i !== index) {
      right.push(arr[i])
    }
  }

  return [...quickSort(left), arr[index], ...quickSort(right)]
}

// 原地快排，时间浮复杂度nlogn 空间复杂度n
function partition(arr, l, r) {
  const index = l
  const init = arr[index]
  l++
  while (l < r) {
    while (arr[r] > init) {
      r--
    }
    while (arr[l] < init) {
      l++
    }
    if (l < r) {
      // 交换位置
      ;[arr[l], arr[r]] = [arr[r], arr[l]]
      l++
      r--
    }
  }

  // 交换一下位置（此时[index + 1, l - 1]都是小于init的，[l, r]都是大于）[4,3,5,6,7] l = 3 r = 2
  ;[arr[index], arr[l - 1]] = [arr[l - 1], arr[index]]
  // [3, 5, 1, 6, 7, 0] init = 3 [1,0,3,6,7,5]
  return l
}
function _quickSort2(arr, start, end) {
  if (start < end) {
    const index = partition(arr, start, end)
    console.log(index, [start, index - 1], [index + 1, end])

    _quickSort2(arr, start, index - 1)
    _quickSort2(arr, index, end)
  }

  return arr
}
function quickSort2(arr) {
  return _quickSort2(arr, 0, arr.length - 1)
}

const arr = [3, 5, 1, 6, 7, 0]
console.log(quickSort2(arr))
