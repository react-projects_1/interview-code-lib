/**
 * 实现思路
   冒泡也是一个比较实现的算法，是一个O(n^2)级别的算法，排序思路如下：
   遍历数组，从第一个位置开始，然后两两互相比较，在第一轮循环结束后，把最大值放到最后的位置，然后循环这个过程，最终把整个数组排序完成。
 */
function bubbleSort(arr) {
  let len = arr.length - 1
  for (let j = 0; j < len; j++) {
    for (let i = 0; i < len - j; i++) {
      if (arr[i] > arr[i + 1]) {
        ;[arr[i], arr[i + 1]] = [arr[i + 1], arr[i]]
      }
    }
  }
  return arr
}
const arr = [3, 5, 1, 6, 7, 0]
console.log(bubbleSort(arr))
