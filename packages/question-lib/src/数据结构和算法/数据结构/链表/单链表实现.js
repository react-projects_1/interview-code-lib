class Node {
  constructor(data, next) {
    this.data = data
    this.next = next
  }
}

/**
 * 引入虚拟头结点出来新增删除，避免多余的空判断
 */
class LinkedList {
  constructor() {
    this.dummyHead = new Node()
    this.size = 0
  }

  getSize() {
    return this.size
  }
  isEmpty() {
    return this.size === 0
  }
  checkAddIndex(index) {
    if (index < 0 || index > this.size) {
      throw new Error('Invalid index: ' + index)
    }
  }
  checkGetIndex(index) {
    if (index < 0 || index >= this.size) {
      throw new Error('Invalid index: ' + index)
    }
  }

  add(index, data) {
    this.checkAddIndex(index)

    let prev = this.dummyHead
    for (let i = 0; i < index; i++) {
      prev = prev.next
    }

    prev.next = new Node(data, prev.next)
    this.size++
  }

  addFirst(data) {
    this.add(0, data)
  }
  addLast(data) {
    this.add(this.size, data)
  }
  get(index) {
    let node = this.dummyHead.next
    for (let i = 0; i < index; i++) {
      node = node.next
    }
    return node.data
  }
  getFirst() {
    return this.get(0)
  }
  getLast() {
    return this.get(this.size - 1)
  }
  set(index, data) {
    this.checkGetIndex(index)
    let node = this.dummyHead.next
    for (let i = 0; i < index; i++) {
      node = node.next
    }
    node.data = data
  }
  contains(data) {
    let node = this.dummyHead.next
    while (node) {
      if (node.data === data) {
        return true
      }
      node = node.next
    }
    return false
  }
  // 删除一个节点，找到待删除的前一个节点，然后出来指针关系
  remove(index) {
    this.checkGetIndex(index)
    // 需要找到待删除的前一个节点
    let prev = this.dummyHead
    for (let i = 0; i < index; i++) {
      prev = prev.next
    }
    const returnNode = prev.next
    if (returnNode) {
      prev.next = returnNode.next
      returnNode.next = null
      this.size--
    }
    return undefined
  }
  removeFirst() {
    return this.remove(0)
  }
  removeLast() {
    return this.remove(this.size - 1)
  }
  removeElement(data) {
    let prev = this.dummyHead
    while (prev.next) {
      if (prev.next.data === data) {
        break
      }
      prev = prev.next
    }

    if (prev.next) {
      const delNode = prev.next
      prev.next = delNode.next
      delNode.next = null
      this.size--
    }
  }
  toString() {
    let str = ''
    let node = this.dummyHead.next
    while (node) {
      if (node.next) {
        str += node.data + ' -> '
      } else {
        str += node.data
      }
      node = node.next
    }
    return str
  }
}
const linked = new LinkedList()
linked.addLast(1)
linked.addLast(2)
linked.addLast(3)
console.log(linked.toString())
