/**
 解法1：n^2级别的时间复杂度
 */
var lengthOfLIS1 = function (nums) {
  const len = nums.length
  if (len <= 1) {
    return len
  }

  const result = Array(len).fill(1) // 默认情况下，每个位置的最大递增序列的长度为1
  // 遍历数组
  for (let i = 0; i < len; i++) {
    // 遍历当前位置前面序号在result中的值，如果大于，说明序列要递增了
    for (let j = 0; j < i; j++) {
      if (nums[i] > nums[j]) {
        result[i] = Math.max(result[i], result[j] + 1)
      }
    }
  }
  // 最终result中就存储着每个位置对应的最长递增子序列的长度
  return Math.max(...result)
}

/**
 解法2：nlogn级别的时间复杂度
 二分法+贪心算法
 关键就是如何二分查找到第一个比值大的那个元素
 */
var lengthOfLIS = function (nums) {
  const len = nums.length
  if (len === 0) return len

  const arr = [nums[0]]
  for (let i = 0; i < len; i++) {
    if (nums[i] > arr[arr.length - 1]) {
      arr.push(nums[i])
    } else {
      // 二分查找出arr中第一个比nums[i]大的元素的索引，并替换成nums[i]
      let left = 0
      let right = arr.length - 1
      while (left < right) {
        const mid = Math.floor((left + right) / 2)
        if (arr[mid] < nums[i]) {
          left = mid + 1
        } else {
          right = mid
        }
      }
      arr[left] = nums[i]
    }
  }
  return arr.length
}

lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18])
