/**
 、
 1 定义一个当前能跳跃的最大下标值cover（默认是0）
 2 然后以cover为边界进行遍历数组nums
 3 遍历 过程中计算出新的最大的cover值
 4 判断新的cover值是否大于等于len - 1，满足就返回true，否则继续遍历计算最大的cover值
 */
var canJump = function (nums) {
  let cover = 0 // 1 表示当前能调到的最大下标值

  // [2,3,1,1,4]
  for (let i = 0; i <= cover; i++) {
    cover = Math.max(cover, i + nums[i]) // i + nums[i]时计算当前可以跳跃的最大步数
    if (cover >= nums.length - 1) {
      // 只要cover大于等于len-1说明可以跳过去了
      return true
    }
  }
  return false
}
