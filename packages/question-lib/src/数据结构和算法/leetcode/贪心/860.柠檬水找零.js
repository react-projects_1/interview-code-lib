/**
 * @param {number[]} bills
 * @return {boolean}
 */
// [5,5,5,10,20] fiveCount 2 tenCount 1
var lemonadeChange = function (bills) {
  let fiveCount = 0
  let tenCount = 0

  for (let i = 0; i < bills.length; i++) {
    const bill = bills[i]
    if (bill === 5) {
      fiveCount++
    } else if (bill === 10) {
      if (fiveCount === 0) {
        return false
      }
      fiveCount -= 1
      tenCount += 1
    } else {
      if (tenCount > 0 && fiveCount > 0) {
        tenCount -= 1
        fiveCount -= 1
      } else if (fiveCount >= 3) {
        fiveCount -= 3
      } else {
        return false
      }
    }
    console.log(fiveCount, tenCount)
  }
  return true
}
lemonadeChange([5, 5, 5, 10, 20])
