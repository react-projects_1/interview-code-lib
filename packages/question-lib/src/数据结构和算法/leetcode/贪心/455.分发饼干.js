/**
 思路：
 1 升序排序
 2 然后遍历胃口值数组，优先去用饼干值满足胃口值最大的小朋友
 3 最后返回结果
 */
var findContentChildren = function (g, s) {
  g = g.sort((a, b) => a - b)
  s = s.sort((a, b) => a - b)
  let count = 0
  let index = s.length - 1
  // 遍历学生胃口值
  g.forEach((item) => {
    // 优先找最大的胃口值去满足，这样算出来的结果就是最优解
    if (index > 0 && s[index] >= item) {
      index--
      count++
    }
  })
  return count
}
