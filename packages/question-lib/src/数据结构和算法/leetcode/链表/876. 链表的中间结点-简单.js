/**
/** 定义两个slow/fast指针，一个走一步，一个走两步，
 * 当fast是尾节点或者为null时，就停止，这是slow刚好就是中间节点
 * @param {ListNode} head
 * @return {ListNode}
 */
var middleNode = function (head) {
  let slow = head
  let fast = head

  while (fast && fast.next) {
    slow = slow.next
    fast = fast.next.next
  }

  return slow
}

/* 
  [1,2,3] slow/fast = 1
  fast && fast.next fast走2步 slow走一步
  [1,2,3,4] slow/fast = 1
*/
