/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * 思路1：使用cache的方式缓存节点
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode1 = function (headA, headB) {
  // 使用缓存方式
  const cache = new Set()

  while (headA) {
    cache.add(headA)
    headA = headA.next
  }

  while (headB) {
    if (cache.has(headB)) {
      return headB
    }
    headB = headB.next
  }
  return null
}

/* 
  1-2-3-4
  2-3-4

*/
/**
 * 前提：相交节点是同一个节点
 * 思路2：使用链表连接在一起进行遍历（定义两个指针）
 *  1-2-3-4
    2-3-4
    连接后变成：
    1-2-3-4-2-3-4
    2-3-4-1-2-3-4
    会发现，如果有相交节点，那么会出现一个值一样的情况
    如果没有，那么遍历完后，两个指针都是null的情况
    所以只要返回其中的一个指针即可
 * @param {*} headA 
 * @param {*} headB 
 */
var getIntersectionNode = function (headA, headB) {
  let a = headA
  let b = headB

  while (a !== b) {
    a = a === null ? headB : a.next
    b = b === null ? headA : b.next
  }
  return a
}
