/**
 * 思路：要删除一个节点，就必须知道这个节点的前一个节点，
 * 定义两个快慢指针，fast快指针先走n步，slow指针不动
 * 然后后面一起走（length-n）步完后，slow指针的位置
 * 刚好就是要删除节点的前一个位置，然后执行prev.next = prev.next.next即可，
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
  const dummyHead = {
    next: head,
  }
  let slow = dummyHead
  let fast = dummyHead
  // [1,2,3,4,5] 删除倒数第二个
  // fast先走两步 fast = 2
  // fast先走n步
  while (n--) {
    fast = fast.next
  }

  // 让slow指针走到待删除节点的前一个节点
  while (fast.next) {
    fast = fast.next
    slow = slow.next
  }
  slow.next = slow.next.next
  return dummyHead.next
}
