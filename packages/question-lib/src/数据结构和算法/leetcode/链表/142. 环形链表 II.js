/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/** 思路：有环，那么在相遇之后呢，重新定义一个从头结点开始的指针，
 * 当slow和新节点相遇时，这个节点就是入环的第一个节点（数学公式推到出来的）
 * 不要问为什么
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function (head) {
  // 还是快慢指针的套路
  let slow = head
  let fast = head

  while (fast) {
    slow = slow.next
    if (fast.next) {
      fast = fast.next.next
    } else {
      // 说明已经到头了，没有环
      return null
    }

    if (fast === slow) {
      // 说明有环
      // 定义一个新指针，当它和slow相遇时，相遇的节点就是入环的第一个节点
      let cur = head
      while (cur !== slow) {
        cur = cur.next
        slow = slow.next
      }
      return cur
    }
  }
  return null
}
