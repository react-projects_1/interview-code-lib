/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} left
 * @param {number} right
 * @return {ListNode}
 */
var reverseBetween = function (head, left, right) {
  const dummyHead = {
    next: head,
  }

  // 1 找到left的前一个节点（方便后面重新连接守卫指针）
  // 然后prevLeft.next.next
  let prevLeft = dummyHead
  for (let i = 0; i < left - 1; i++) {
    prevLeft = prevLeft.next
  }

  // 翻转left-right的链表
  let prev = null
  let current = prevLeft.next
  for (let j = 0; j < right - left + 1; j++) {
    const next = current.next
    current.next = prev
    prev = current
    current = next
  }

  // prevLeft.next.next = left位置的节点
  // 需要把left.next -> right + 1对应的节点位置
  prevLeft.next.next = current
  // 需要把left - 1.next -> right的位置
  prevLeft.next = prev

  return dummyHead.next
}
