/** 递归写法，定义：删除表头中值等于val的元素，并返回新的头节点 */
var removeElements = function (head, val) {
  if (!head) {
    return head
  }

  head.next = removeElements(head.next, val)
  return head.val === val ? head.next : head
}

/** 非递归写法（引入虚拟头结点的方式，避免空指针判断）这是运行最快的方式 */
var removeElements = function (head, val) {
  const dummyHead = { next: head }
  let prev = dummyHead
  while (prev.next) {
    if (prev.next.val === val) {
      // 找到元素就需要删除
      const delNode = prev.next
      prev.next = delNode.next
      delNode.next = null
    } else {
      prev = prev.next
    }
  }

  return dummyHead.next
}

/** 非递归写法（不引入虚拟头结点的方式，直接删除） */
var removeElements = function (head, val) {
  if (!head) {
    return head
  }
  let prev = head
  // 遍历头结点后的所有元素
  while (prev.next) {
    if (prev.next.val === val) {
      // 找到元素就需要删除
      const delNode = prev.next
      prev.next = delNode.next
      delNode.next = null
    } else {
      prev = prev.next
    }
  }

  // 需要额外处理是否需要删除头结点
  return head.val === val ? head.next : head
}
