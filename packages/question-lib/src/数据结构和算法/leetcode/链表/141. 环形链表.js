/**
 * 解法1：定义一个缓存表，如果有环，那么在后续的遍历中，一定会出现一个节点和缓存
 * 表里的一致
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function (head) {
  const set = new Set()
  let node = head
  while (node) {
    if (set.has(node)) {
      return true
    }
    set.add(node)
    node = node.next
  }
  return false
}

/**
 * 解法2：快慢指针，也就是说如果是环形链表，那么快慢指针一定会有重合的一次，通过这个原理
 * 就可以判断是否是环形链表
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function (head) {
  let fast = head
  let slow = head
  while (fast && fast.next) {
    fast = fast.next.next
    slow = slow.next
    if (fast === slow) return true
  }
  return false
}
