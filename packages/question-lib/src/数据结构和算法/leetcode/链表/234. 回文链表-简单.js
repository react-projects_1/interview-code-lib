/**
 * 思路：
 * 回文链表的特点是对称性，那么我们可以先把前面一半的节点
 * 翻转过来，然后从中间开始往两边遍历，如果是回文链表，那么
 * 每个对应的值都应该是相等的，不过这里需要特殊处理链表
 * 长度是奇偶数的情况
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function (head) {
  // slow遍历后，它的位置就在中间节点的位置
  let slow = head
  let fast = head
  // 翻转中间节点的前面链表
  let prev = null

  // 这里就是找到中间节点的方法，同时翻转链表
  while (fast && fast.next) {
    fast = fast.next.next
    const next = slow.next
    slow.next = prev
    prev = slow
    slow = next
  }

  if (fast) {
    // 说明链表长度是奇数，slow需要往前一步，因为中间节点
    // 不需要比较是否相同
    slow = slow.next
  }

  while (prev && slow) {
    if (prev.val !== slow.val) {
      return false
    }
    prev = prev.next
    slow = slow.next
  }
  return true
}

/* 
  [1,2,3] 中间节点=2
  [2,3,4,5] 中间点击=4
*/
