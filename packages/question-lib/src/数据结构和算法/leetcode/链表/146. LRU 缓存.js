/* 
  LRU：least recently used 最近最少使用
  
  假设有[1,2,3]（最前面的是最先访问的，当需要删除数据时，需要删除最前面的数据）
  情况1：[1,2,3]当访问到了2，那么需要将容器里的2先删除，在重新插入到尾部[1,3,2]
  情况2：[1,3,2]当插入4，发现没有4，那么会先删除1在插入4，变成[3,2,4]
  情况3：[3,2,4]当插入3，发现存在，那么会先删除容器里的3，在队尾在插入3，变成[2,4,3]
  总结：就是经常访问到的元素会放置到队尾，保证删除的时候有限删除队首的元素（也就是不经常访问到的元素）
*/
/**
 * 实现思路：
 * 1 利用map实现链表效果（map.keys()会顺序返回添加的key值的一个迭代器对象）
 * 2 在获取数据的时候，如果存在，需要新删除数据，在更新最新的位置（[1,2,3]，在返回2的时候，需要变更为[1,3,2]）
 * 3 在添加数据的时候，有两种情况
 * 情况1=数据已存在：需要将之前的数据删除，在重新插入
 * 情况2-数据不存在：如果容积已满，那么需要先删除第一个数据，在插入新的数据，没满就直接插入数据即可
 * @param {*} capacity
 */
var LRUCache = function (capacity) {
  this.capacity = capacity
  this.map = new Map()
}

/**
 * @param {number} key
 * @return {number}
 */
LRUCache.prototype.get = function (key) {
  if (this.map.has(key)) {
    const value = this.map.get(key)
    // 需要将访问的数据更新到最前面为止
    this.map.delete(key)
    this.map.set(key, value)
    return value
  }
  return -1
}

/**
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
LRUCache.prototype.put = function (key, value) {
  if (this.map.has(key)) {
    // 先删除
    this.map.delete(key)
    // 在添加到最新的位置
    this.map.set(key, value)
  } else {
    if (this.map.size >= this.capacity) {
      this.map.delete(this.map.keys().next().value)
    }
    this.map.set(key, value)
  }
}

const map = new Map()
map.set(1, 1)
map.set(2, 2)
map.set(3, 3)
map.delete(2)
map.set(2, 2)
let str = ''
const keys = map.keys()
let node = keys.next()
console.log(node, str)

while (!node.done) {
  str += node.value + ' -> '
  node = keys.next()
}
console.log(node, str)

// 应用场景：在vue中的keep-alive组件中会使用到
