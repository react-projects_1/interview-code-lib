# 快慢指针找是否有环

# 快慢指针找中间节点

```javascript
let slow = head
let fast = head

while (fast && fast.next) {
  slow = slow.next
  fast = fast.next.next
}
return slow // slow就是中间节点
```

# 两个指针翻转链表结构
