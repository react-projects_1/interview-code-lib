/**
 * 遍历两个链表，然后把值小的那个添加到临时链表里
 */
/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
var mergeTwoLists = function (list1, list2) {
  const dummyHead = {
    next: null,
  }

  let temp = dummyHead
  while (list1 && list2) {
    if (list1.val < list2.val) {
      temp.next = list1
      list1 = list1.next
    } else {
      temp.next = list2
      list2 = list2.next
    }
    // 指针后移，方便下层循环继续添加最小节点
    temp = temp.next
  }
  // 遍历完后，list1/list2可能不为空，所以需要继续添加
  temp.next = list1 || list2

  return dummyHead.next
}
