/**
 * 思路：使用一个栈存储字符，然后遍历字符串，如果当前字符和栈顶元素一样，那么需要删除栈顶元素。否则添加到栈中，
 * 最后返回栈里的数据
 * @param {string} s
 * @return {string}
 */
var removeDuplicates = function (s) {
  const stack = []

  for (const ch of s) {
    const len = stack.length
    const lastCh = stack[len - 1]

    if (len && ch === lastCh) {
      stack.pop()
    } else {
      stack.push(ch)
    }
  }
  return stack.join('')
}
