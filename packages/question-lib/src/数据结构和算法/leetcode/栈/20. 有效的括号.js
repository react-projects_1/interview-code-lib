/**
 * @param {string} s
 * @return {boolean}
 */
for (let k in '111') {
  console.log(k)
}
console.log('111'[1])

var isValid = function (s) {
  const table = {
    '(': ')',
    '[': ']',
    '{': '}',
  }
  const stack = []
  for (let i = 0; i < s.length; i++) {
    let ele = s[i]
    if (ele in table) {
      stack.push(ele)
    } else if (ele !== table[stack.pop()]) {
      // 这里需要注意：不等于table中pop的值对用的右匹配，那么就不是一个有效的匹配
      return false
    }
  }
  return stack.length === 0
}
