/**
 * @param {string} path
 * @return {string}
 */
var simplifyPath = function (path) {
  const stack = []
  const paths = path.split('/')

  for (let p of paths) {
    // 说明需要返回上一级目录，所以需要出站
    if (p === '..') {
      stack.pop()
    } else if (p && p !== '.') {
      // 正常目录，进站
      stack.push(p)
    }
  }
  return '/' + stack.join('/')
}
