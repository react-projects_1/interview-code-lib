/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function (tokens) {
  const map = {
    '+': (a, b) => a + b,
    '-': (a, b) => b - a,
    '*': (a, b) => a * b,
    '/': (a, b) => getNum(b / a),
  }
  const stack = []

  for (const x of tokens) {
    if (x in map) {
      const result = map[x](stack.pop(), stack.pop())
      stack.push(result)
    } else {
      stack.push(Number(x))
    }
    // ["2","1","+","3","*"]
  }
  return stack.pop()
}

// 去掉小数点后的数字
function getNum(num) {
  return num | 0
}
