var MyQueue = function () {
  this.stackIn = [] // 临时保存元素的栈
  this.stackOut = [] // 取元素的栈
}

/**
 * @param {number} x
 * @return {void}
 */
MyQueue.prototype.push = function (x) {
  this.stackIn.push(x)
}

/**
 * @return {number}
 */
MyQueue.prototype.pop = function () {
  if (!this.stackOut.length) {
    // 转移数据
    while (this.stackIn.length) {
      this.stackOut.push(this.stackIn.pop())
    }
  }
  return this.stackOut.pop()
}

/**
 * @return {number}
 */
MyQueue.prototype.peek = function () {
  const ele = this.pop()
  this.stackOut.push(ele)
  return ele
}

/**
 * @return {boolean}
 */
MyQueue.prototype.empty = function () {
  return !this.stackIn.length && !this.stackOut.length
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * var obj = new MyQueue()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.peek()
 * var param_4 = obj.empty()
 */
