/**
 * 使用回溯的方式解决问题
 */
/**
 *
 * @param {*} result 收集结果（输出）
 * @param {*} tempList 临时结果（临时结果）
 * @param {*} nums 数据（输入）
 * @returns
 */
function find(result, tempList, nums) {
  if (tempList.length === nums.length) {
    // 找到了一个结果，存进去
    return result.push([...tempList])
  }

  for (let i = 0; i < nums.length; i++) {
    // 找到这一个元素不在tempList里，可以继续递归放置
    if (!tempList.includes(nums[i])) {
      tempList.push(nums[i]) // 放进去一个元素
      find(result, tempList, nums) // 递归继续查询
      tempList.pop() //撤回这个元素
    }
  }
}
var permute = function (nums) {
  let result = []
  find(result, [], nums)
  return result
}
permute([1, 2, 3])
