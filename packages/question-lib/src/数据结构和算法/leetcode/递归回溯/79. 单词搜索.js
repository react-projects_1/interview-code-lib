/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
  const row = board.length
  const column = board[0].length
  for (let i = 0; i < row; i++) {
    for (let j = 0; j < column; j++) {
      const result = find(i, j, 0)
      if (result) {
        return result
      }
    }
  }

  // 查找board的[i][j]位置上的字母是否与word的cur位置字母匹配
  function find(i, j, cur) {
    // 1 检查是否越界
    if (i >= row || i < 0) {
      return false
    }
    if (j >= column || j < 0) {
      return false
    }
    const ch = board[i][j]
    if (ch !== word[cur]) {
      return false
    }
    if (cur === word.length - 1) {
      return true // 说明找到了
    }

    board[i][j] = null // 设置为空，是让不要重复返回匹配这个已经匹配过的位置
    // 否则继续递归上下左右四个位置继续查找
    const result =
      find(i, j - 1, cur + 1) ||
      find(i, j + 1, cur + 1) ||
      find(i - 1, j, cur + 1) ||
      find(i + 1, j, cur + 1)
    board[i][j] = ch
    return result
  }

  return false
}
