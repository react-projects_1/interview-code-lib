/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
// O(n^2)复杂度
var twoSum = function (nums, target) {
  for (var i = 0; i < nums.length; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] + nums[j] === target) {
        return [i, j]
      }
    }
  }
}

// 解法2：时间复杂度变成O(n)，空间复杂度O(n)，采用空间换时间的方式
var twoSum2 = function (nums, target) {
  const table = {} // {2: 1} // 存储已访问过元素的下标值，方便后续匹配
  for (let i = 0; i < nums.length; i++) {
    const num = target - nums[i]
    if (table[num] !== undefined) {
      return [i, table[num]]
    }
    table[nums[i]] = i
  }
}
