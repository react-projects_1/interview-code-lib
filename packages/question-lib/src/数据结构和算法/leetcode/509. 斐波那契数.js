// 递归写法-耗内存
var fib1 = function (n) {
  if (n <= 1) return n

  return fib(n - 1) + fib(n - 2)
}

// 递归写法-缓存写法
var fib2 = function (n) {
  const _fib = (n, memo = {}) => {
    if (n <= 1) return n
    if (memo[n]) {
      return memo[n]
    }
    const result = _fib(n - 1, memo) + _fib(n - 2, memo)
    memo[n] = result
    return result
  }
  return _fib(n)
}

// 动态规划写法
var fib = function (n) {
  if (n <= 1) return n
  let dp1 = 0
  let dp2 = 1
  let dp3

  for (let i = 2; i <= n; i++) {
    dp3 = dp1 + dp2
    dp1 = dp2
    dp2 = dp3
  }
  return dp3
}
