/* 
  常规做法
*/
var isPowerOfTwo = function (n) {
  let result = n
  while (result) {
    if (result === 1) {
      return true
    }
    if (result % 2 === 1) {
      return false
    }
    result = result / 2
  }
  return false
}

/* 异或位运算做法 
  分析：只要一个整数是2的整次幂，那么它的特点就是二进制表示只有一个1和其他的0
  比如：n = 4 0x100，n - 1 = 3 0x011;也就是说如果一个n是整数，那么n & n - 1 = 0
  0x001 ^ 0x000
*/
var isPowerOfTwo = function (n) {
  return n > 0 && (n & (n - 1)) === 0
}
