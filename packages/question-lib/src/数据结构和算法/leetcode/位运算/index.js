// << >> 位运算左移和右移

console.log(1 << 2) // 1 * 2 ^ 2 = 4
console.log(1 << 1) // 1 * 2 ^ 1 = 2

console.log(9 >> 2) // 1001 >> 2 = 0010 = 2

// |符号（按位或 0x10 | 0x01 = 0x11）：作用就是用来授权的
// &符号（按位与 0x11 | 0x01 = 0x01）：用来判断是否拥有某个权限
// ^符号（按位异或（只要不同才返回1，相同返回0） 0x11 | 0x00 = 0x11）：用来删除某个权限

const Text = 1
const Func = 1 << 1
const Component = 1 << 2
let types = Text | Func // 同时拥有两种属性
const isText = (types & Text) === Text // 判断是否有某个权限
types ^= Text // 去掉Text权限
