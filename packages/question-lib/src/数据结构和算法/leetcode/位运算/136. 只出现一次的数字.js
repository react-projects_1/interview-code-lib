// 对于位运算的异或来说，只要对应位上的数字不一样才等于1，否则结果是0
// 根据这个特性可以知道任何一个数字和0异或的结果还是它本身，两个相同的数字异或等于0
// 0 ^ 0x11 = 0x11 或 0x11 ^ 0x11 = 0
// 同时异或运算满足交换律和结合律，也就是a⊕b⊕a = b⊕a⊕a = b⊕a⊕a，根据这个特性，那么如果
// 这个数只出现一次，那么只要在数组的的数字都异或一遍，最终的结果就是值出现一次的那个值

/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function (nums) {
  let result = 0
  nums.forEach((num) => {
    result ^= num
  })
  return result
}

/**
 * 不用位运算写法，用map记录次数
 * @param {*} nums
 * @returns
 */
var singleNumber = function (nums) {
  const map = {}
  nums.forEach((num) => {
    map[num] = map[num] ? map[num] + 1 : 1
  })
  const keys = Object.keys(map)
  let result
  keys.forEach((key) => {
    if (map[key] === 1) {
      result = key
    }
  })

  return result
}
