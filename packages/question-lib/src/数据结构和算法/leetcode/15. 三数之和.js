/**
 1 数组排序（保证不重复的前提）
 2 遍历数组
 3 先固定一个数nums[i]，然后使用两个左右指针L/R
 4 如果nums[i] > 0 说明肯定不符合跳出循环（以为数组已经是按照从小到大排序了）
 5 去重：如果nums[i] === nums[i - 1]需要去重
 6 当sum === 0，nums[l] === nums[l+1]，会导致结果重复，需要跳过所以需要l++
 7 当sum === 0，nums[r] === nums[r - 1]，会导致结果重复，需要跳过所以需要r--
 */
var threeSum = function (nums) {
  const list = []
  nums.sort((a, b) => a - b)

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > 0) {
      break
    }
    if (nums[i] === nums[i - 1]) {
      continue
    }
    let left = i + 1
    let right = nums.length - 1
    while (left < right) {
      const sum = nums[i] + nums[left] + nums[right]
      if (sum === 0) {
        list.push([nums[i], nums[left], nums[right]])
        while (nums[left] === nums[left + 1]) left++
        while (nums[right] === nums[right - 1]) right--
        left++
        right--
      } else if (sum > 0) {
        right--
      } else {
        left++
      }
    }
  }
  return list
}
