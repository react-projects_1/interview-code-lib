/** 双层循环暴力解法
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function (target, nums) {
  let len = nums.length
  let result = len + 1
  // [2,3,1,2,4,3]
  for (let i = 0; i < len; i++) {
    let sum = 0
    for (let j = i; j < len; j++) {
      sum += nums[j]
      if (sum >= target) {
        result = Math.min(j - i + 1, result)
        break
      }
    }
  }
  return result === len + 1 ? 0 : result
}

/*
  滑动窗口解法
*/

var minSubArrayLen2 = function (target, nums) {
  let len = nums.length
  let sum = 0
  let result = Infinity
  let slow = 0
  let fast = 0
  while (fast < len) {
    sum += nums[fast]
    while (sum >= target) {
      // 说明slow-fast之间就是满足条件的
      result = Math.min(result, fast - slow + 1)
      // 然后slow指针前移，继续找看是否是最小的
      sum -= nums[slow]
      slow++
    }
    fast++
  }

  return result === Infinity ? 0 : result
}
