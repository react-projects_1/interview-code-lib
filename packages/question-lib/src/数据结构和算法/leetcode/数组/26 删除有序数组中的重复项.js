/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
  // 前提数组已经是有序数组
  // 慢指针只有发现不同元素时才往前走一步(指向不重复的那个元素)
  // 所以长度 = slow + 1
  let slow = 0
  let fast = 0 // 快指针每次都往前走一步
  const len = nums.length
  // [1,1,2,2,3]
  while (fast < len) {
    if (nums[fast] !== nums[slow]) {
      slow++
      const temp = nums[fast]
      nums[fast] = nums[slow]
      nums[slow] = temp
    }
    fast++
  }
  return slow + 1
}
/* 
  1 返回不重复元素的个数k
  2 同时修改原数组，保证前k个原生不重复
*/
const arr = [1, 1, 2, 2, 3]
// 3 [1,2,3,2,1]
console.log(removeDuplicates(arr), arr)
