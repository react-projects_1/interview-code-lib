/**
 * 特点：如果有负数，那么都是结果是先递减在递增，也就是左右两边的是最大值
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function (nums) {
  let len = nums.length
  let left = 0
  let right = len - 1
  let arr = Array(len)
  let index = right

  while (left <= right) {
    const l = nums[left] * nums[left]
    const r = nums[right] * nums[right]

    if (r > l) {
      arr[index] = r
      right--
    } else {
      arr[index] = l
      left++
    }
    index--
  }
  return arr
}
