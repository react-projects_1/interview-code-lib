/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function (nums, val) {
  if (nums.length === 0) return 0
  const len = nums.length
  let k = 0

  for (let i = 0; i < len; i++) {
    if (nums[i] !== val) {
      nums[k++] = nums[i]
    }
  }

  return k
}
/* 
[3,2,2,3] => [2,2,3,3]
*/
