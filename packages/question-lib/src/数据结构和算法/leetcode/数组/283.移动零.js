/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  let slow = 0
  let fast = 0
  let len = nums.length
  while (fast < len) {
    if (nums[fast]) {
      // eslint-disable-next-line @typescript-eslint/no-extra-semi
      ;[nums[slow], nums[fast]] = [nums[fast], nums[slow]]
      slow++
    }

    fast++
  }
}

// [0,1,0,3,12]
