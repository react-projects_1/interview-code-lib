/**
 * 使用双指针的方式进行处理空格和单词
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
  let left = 0
  let right = s.length - 1
  let word = ''
  const list = []

  while (s[left] === ' ') {
    left++
  }
  while (s[right] === ' ') {
    right--
  }

  while (left <= right) {
    if (word && s[left] === ' ') {
      // 说明已经是单词的结尾了，需要收集起来
      list.unshift(word)
      word = ''
    } else if (s[left] !== ' ') {
      word += s[left]
    }
    left++
  }
  list.unshift(word)
  return list.join(' ')
}
