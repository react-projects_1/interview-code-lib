/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
  const result = []
  if (root == null) return result
  const queue = [root]

  while (queue.length) {
    const levelArr = []
    // 记录每一层的节点个数，方便等下遍历（因为遍历的时候会每次都会往queue里面追加数据）
    let levelLen = queue.length
    // 取出当前层的数据
    while (levelLen--) {
      const node = queue.shift()
      levelArr.push(node.val)
      // 遍历的过程中把下一层的数据添加到队列里面
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
    result.push(levelArr)
  }
  return result
}
