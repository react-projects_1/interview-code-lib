/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：只要发现是左叶子节点，加把值累加即可
 * @param {TreeNode} root
 * @return {number}
 */
var sumOfLeftLeaves = function (root) {
  let sum = 0

  function dfs(node) {
    if (node === null) return

    const leftNode = node.left
    if (leftNode && leftNode.left === null && leftNode.right === null) {
      sum += leftNode.val
    }
    dfs(node.left)
    dfs(node.right)
  }

  dfs(root)

  return sum
}
