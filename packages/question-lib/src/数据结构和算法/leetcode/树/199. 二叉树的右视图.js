/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：其实就是可以转换成每层的最右节点组成的顺序就是右视图
 * @param {TreeNode} root
 * @return {number[]}
 */
var rightSideView = function (root) {
  let result = []
  if (root === null) return result
  const queue = [root]
  while (queue.length) {
    let levelLen = queue.length
    while (levelLen--) {
      const node = queue.shift()
      if (levelLen === 0) {
        result.push(node.val)
      }
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
  }
  return result
}
