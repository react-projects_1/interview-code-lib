/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function (p, q) {
  const travese = (node1, node2) => {
    if (node1 === null && node2 === null) {
      return true
    }
    if (node1 === null || node2 === null) {
      return false
    }

    // 看看左右子树是否相同
    const sameLeft = travese(node1.left, node2.left)
    const sameRight = travese(node1.right, node2.right)

    if (node1.val === node2.val && sameLeft && sameRight) {
      return true
    }
    return false
  }

  return travese(p, q)
}
