/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function (root) {
  /**
   * 判断
   * @param {*} node1
   * @param {*} node2
   * @returns
   */
  const travese = (node1, node2) => {
    if (node1 === null && node2 === null) {
      return true
    }
    if (node1 === null || node2 === null) {
      return false
    }

    // 1 值要相等
    return (
      node1.val === node2.val &&
      travese(node1.left, node2.right) &&
      travese(node1.right, node2.left)
    )
  }

  return travese(root.left, root.right)
}
