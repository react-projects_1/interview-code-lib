/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var largestValues = function (root) {
  let result = []
  if (root === null) return result

  const queue = [root]

  while (queue.length) {
    let levelLen = queue.length
    let maxVal = queue[0]
    while (levelLen--) {
      const node = queue.shift()
      maxVal = maxVal > node.val ? maxVal : node.val
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
    result.push(maxVal)
  }

  return result
}
