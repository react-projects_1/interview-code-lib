/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  const arr = []
  const dfs = (node) => {
    if (node === null) {
      return
    }
    arr.push(node.val)
    dfs(node.left)
    dfs(node.right)
  }

  dfs(root)
  return arr
}

// 层序遍历
var preorderTraversal = function (root) {
  const arr = []
  if (root === null) {
    return arr
  }
  const stack = [root]
  while (stack.length) {
    const node = stack.pop()
    arr.push(node.val)
    node.right && stack.push(node.right)
    node.left && stack.push(node.left)
  }
  return arr
}
