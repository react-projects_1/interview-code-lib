/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：先计算出左右子树的节点个数，然后+1就是当前的节点对应的节点个数
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function (root) {
  if (root === null) {
    return 0
  }

  const left = countNodes(root.left)
  const right = countNodes(root.right)

  return left + right + 1
}
