/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：找到链表的中间节点，作为根节点的内容，然后左右链表各自构成左右子树的节点
 * @param {ListNode} head
 * @return {TreeNode}
 */
var sortedListToBST = function (head) {
  if (!head) return head
  function help(head, tail) {
    // 如果指针相遇了说明构建结束了
    if (head === tail) return null

    const media = getMedia(head, tail)

    const root = new TreeNode(media.val)
    root.left = help(head, media)
    root.right = help(media.next, tail)
    return root
  }

  // 左必右开的链表
  return help(head, null)
}

function getMedia(head, tail) {
  let slow = head
  let fast = head

  while (fast !== tail && fast.next !== tail) {
    slow = slow.next
    fast = fast.next.next
  }
  return slow
}

function TreeNode(val, left, right) {
  this.val = val === undefined ? 0 : val
  this.left = left === undefined ? null : left
  this.right = right === undefined ? null : right
}
