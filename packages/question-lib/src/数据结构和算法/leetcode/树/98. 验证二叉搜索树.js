/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：利用二分搜索树，中序遍历一定是从小到大排序的结果，那么可以定义一个prev值，然后每次访问时和当前值进行比较
 * 如果当前值都小于prev值，说明就不是二分搜索树，直接返回false
 * @param {TreeNode} root
 * @return {boolean}
 */
var isValidBST = function (root) {
  let prev = -Infinity
  function dfs(node) {
    if (node === null) {
      return true
    }

    const left = dfs(node.left)

    if (node.val < prev) {
      return false
    }

    prev = node.val
    const right = dfs(node.right)
    return left && right
  }

  return dfs(root)
}
