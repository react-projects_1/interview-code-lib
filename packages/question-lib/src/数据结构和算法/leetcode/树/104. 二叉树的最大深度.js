// 计算root的最大深度（公式：最大深度 = node.left最大深度 + 1）
var maxDepth = function (root) {
  if (!root) {
    return 0
  }

  return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1
}
