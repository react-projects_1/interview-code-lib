/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 解题思路：
 * 1 先定义一个判断是否是相同树的工具方法
 * 2 然后比较如果root和subRoot的值相同，那么就用判断是否是相同树的方法判断，如果是相同的树，就返回true，
 * 否则继续递归拿左右子树去查找是否包含subRoot
 * @param {TreeNode} root
 * @param {TreeNode} subRoot
 * @return {boolean}
 */
var isSubtree = function (root, subRoot) {
  if (root === null) {
    return false
  }

  if (root.val === subRoot.val) {
    // 这时可以比较以这两个为根节点的树是否是同一颗树，是就直接返回，否则继续在左右子树判断是否包含子树
    if (isSameTree(root, subRoot)) {
      return true
    }
  }

  return isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot)
}

function isSameTree(root1, root2) {
  if (root1 === null && root2 === null) {
    return true
  }
  if (root1 === null || root2 === null) {
    return false
  }

  // 都不为null
  if (root1.val !== root2.val) {
    return false
  }
  return isSameTree(root1.left, root2.left) && isSameTree(root1.right, root2.right)
}
