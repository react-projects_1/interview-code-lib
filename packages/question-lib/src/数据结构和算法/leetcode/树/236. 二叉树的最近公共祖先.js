/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * 定义：如果p或者q是root节点或者在root节点的左右子树，则返回true
 * 那么最近的公共祖先一定满足的条件是：
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  if (!root) {
    return root
  }

  if (root === p || root === q) {
    return root
  }
  const inLeft = lowestCommonAncestor(root.left, p, q)
  const inRight = lowestCommonAncestor(root.right, p, q)
  if (inLeft && inRight) {
    // 说明root就是最近的公共祖先了
    return root
  }
  return inLeft ? inLeft : inRight
}
