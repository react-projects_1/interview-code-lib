/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：需要采用前序遍历的方式，然后遍历到叶子节点的时候需要把结果保存起来，同时维护一个中间变量去处理中间值
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function (root) {
  const result = []
  if (root === null) {
    return result
  }

  function dfs(node, path) {
    if (node === null) {
      return
    }
    if (node.left === null && node.right === null) {
      // 说明到达叶子几点需要继续收集结果
      path.push(node.val)
      result.push(path.join('->'))
      return
    }
    dfs(node.left, [...path, node.val])
    dfs(node.right, [...path, node.val])
  }

  dfs(root, [])
  return result
}
