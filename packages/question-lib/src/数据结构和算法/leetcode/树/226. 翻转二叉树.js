// 计算root的最大深度（公式：最大深度 = node.left、right中的最大深度值 + 1）深度优先遍历
var invertTree = function (root) {
  if (!root) {
    return root
  }
  // 第一步：先交换当前节点的左右子树
  const temp = root.left
  root.left = root.right
  root.right = temp

  // 递归的交换当前节点 左子树
  invertTree(root.left)
  // 递归的交换当前节点 右子树
  invertTree(temp)
  return root
}
