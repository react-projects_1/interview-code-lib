/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 实现方案：就是找到当前节点为准，左右子树最大深度相加在加1，最后在减调1就是最长路径了
 * 采用后续遍历的方式，先计算出左右子树的深度，那么当前节点连接左右子树的最大路径对应的深度=leftDeep + rightDeep + 1
 * @param {TreeNode} root
 * @return {number}
 */
var diameterOfBinaryTree = function (root) {
  // 定义以root为节点，左右最长子树连起来的单向树的深度
  let len = 1

  function dfs(node) {
    if (!node) return 0
    const leftDeep = dfs(node.left)
    const rightDeep = dfs(node.right)
    len = Math.max(len, leftDeep + rightDeep + 1)
    return Math.max(leftDeep, rightDeep) + 1
  }

  dfs(root)
  return len - 1
}
