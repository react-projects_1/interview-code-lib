/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 思路：先中序遍历得到一个原本递增的数组，然后遍历数组，找到被调换位置的两个节点，并交换值即可
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */
var recoverTree = function (root) {
  if (root === null) return root

  let list = []
  let first = null
  let second = null

  function dfs(node) {
    if (node === null) return
    dfs(node.left)
    list.push(node)
    dfs(node.right)
  }
  dfs(root)

  for (let i = 0; i < list.length - 1; i++) {
    if (list[i].val > list[i + 1].val) {
      // 说明找到位置
      if (!first) {
        first = list[i]
      }
      second = list[i + 1]
    }
  }
  if (first && second) {
    const temp = first.val
    first.val = second.val
    second.val = temp
  }

  return root
}
