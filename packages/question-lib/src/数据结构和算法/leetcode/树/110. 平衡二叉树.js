/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * 平衡二叉树的定义：每个节点的左右子树的深度差小于等于1
 *
 * @param {TreeNode} root
 * @return {boolean}
 */
var isBalanced = function (root) {
  /**
   * 计算当前root节点的高度
   * -1表示左右子树的高度差超过了1
   * @param {*} root
   */
  function depth(root) {
    if (root === null) {
      return 0
    }
    const left = depth(root.left)
    const right = depth(root.right)
    // 这时左右子树的高度都计算出来了,可以计算差值了

    if (left === -1 || right === -1) {
      return -1
    }

    if (Math.abs(left - right) > 1) {
      return -1
    }
    return Math.max(left, right) + 1
  }

  return depth(root) !== -1
}
