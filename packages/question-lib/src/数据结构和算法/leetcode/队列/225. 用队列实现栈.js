var MyStack = function () {
  this.queue1 = [] // 临时存储数据
  this.queue2 = []
}

/**
 * @param {number} x
 * @return {void}
 */
MyStack.prototype.push = function (x) {
  this.queue1.push(x)
}

/**
 * @return {number}
 */
MyStack.prototype.pop = function () {
  /* 
    q1: [1]直接弹出 [1,2,3]
    q2: [1,2]
  */
  if (!this.queue1.length && this.queue2.length) {
    // 需要将queue2的数据全部移动到queue1，方便后面取数据
    this.queue1 = [...this.queue2]
    this.queue2 = []
  }

  // queue1出队，保证只剩下一个元素，也就是栈顶元素
  while (this.queue1.length > 1) {
    //
    this.queue2.push(this.queue1.shift())
  }
  return this.queue1.shift()
}

/**
 * @return {number}
 */
MyStack.prototype.top = function () {
  const ele = this.pop()
  this.push(ele)
  return ele
}

/**
 * @return {boolean}
 */
MyStack.prototype.empty = function () {
  return !this.queue1.length && !this.queue2.length
}

/**
 * Your MyStack object will be instantiated and called as such:
 * var obj = new MyStack()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.empty()
 */
