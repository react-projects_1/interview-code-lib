import {
  scheduleCallback,
  ImmediatePriority as ImmediateSchedulerPriority,
  UserBlockingPriority as UserBlockingSchedulerPriority,
  NormalPriority as NormalSchedulerPriority,
  IdlePriority as IdleSchedulerPriority,
  shouldYield,
  Scheduler_cancelCallback,
  now,
} from './Scheduler'
import { createWorkInProgress } from './ReactFiber'
import { beginWork } from './ReactFiberBeginWork'
import { completeWork } from './ReactFiberCompleteWork'
import { NoFlags, MutationMask, Update, Placement, ChildDeletion, Passive } from './ReactFiberFlags'
import {
  commitMutationEffectsOnFiber,
  commitPassiveUnmountEffects,
  commitPassiveMountEffects,
  commitLayoutEffects,
} from './ReactFiberCommitWork'
import { finishQueueingConcurrentUpdates } from './ReactFiberConcurrentUpdates'
import { FunctionComponent, HostComponent, HostRoot, HostText } from './ReactWorkTags'
import {
  markRootUpdated,
  NoLanes,
  NoLane,
  getNextLanes,
  getHighestPriorityLane,
  SyncLane,
  includesBlockingLane,
  NoTimestamp,
  markStarvedLanesAsExpired,
  mergeLanes,
  markRootFinished,
  includesExpiredLane,
} from './ReactFiberLane'
import { getCurrentEventPriority } from 'react-dom-bindings/src/client/ReactDOMHostConfig'
import {
  ContinuousEventPriority,
  DefaultEventPriority,
  DiscreteEventPriority,
  IdleEventPriority,
  getCurrentUpdatePriority,
  lanesToEventPriority,
  setCurrentUpdatePriority,
} from './ReactEventPriorities'
import { flushSyncCallbacks, scheduleSyncCallback } from './ReactFiberSyncTaskQueue'

let workInProgress = null // 正在处理的fiber节点（需要构建子fiber链表）
let workInProgressRoot = null // 正在构建中的根节点
// root 上是否有effect副作用
let rootDoesHavePassiveEffects = false // 根节点上是否有useEffect副作用
// 具有useEffect副作用的根节点 FiberRootNode,根fiber.stateNode
let rootWithPendingPassiveEffects = null
let workInProgressRootRenderLanes = NoLanes // 根节点正在渲染的lanes级别

const RootInProgress = 0 // 表示fiber树正在进行中（构建）
const RootCompleted = 5 // 构建fiber树已经完成
// 当渲染工作结束的时候当前的fiber树处于什么状态,默认进行中
let workInProgressRootExitStatus = RootInProgress

//保存当前的事件发生的时间
let currentEventTime = NoTimestamp

// 这个方法会被调用多次（）比如useState
/**
 * 从root上开始调度更新
 * @param {*} root
 * @param {*} fiber
 * @param {*} lane
 */
export function scheduleUpdateOnFiber(root, fiber, lane, eventTime) {
  // 给root标记有更新级(累加lane的更新，因为可能会有并发的更新到来)
  markRootUpdated(root, lane) // 就是标记当前root上有哪些更新优先级lanes
  // 确保调度执行root上的更新
  ensureRootIsScheduled(root, eventTime)
}

function ensureRootIsScheduled(root, currentTime) {
  //先获取当前根上执行任务
  const existingCallbackNode = root.callbackNode
  // 把所有饿死的赛道标记为过期赛道（这样当赛道饿死过期后，会以同步的方式直接更新这个赛道对应的任务）
  // 同时给新的lane标记上过期时间（这样当下次更新时，就可以找到哪些lane是过期的）
  markStarvedLanesAsExpired(root, currentTime)
  //获取当前优先级最高的车道
  const nextLanes = getNextLanes(root, workInProgressRootRenderLanes)
  // 如果当前的调度没有更新，则停止继续调度
  if (nextLanes === NoLanes) {
    return
  }
  //获取新的调度优先级
  const newCallbackPriority = getHighestPriorityLane(nextLanes)
  // 获取当前根正在运行的优先级
  const existingCallbackPriority = root.callbackPriority
  // 如果新的优先级和老的优先级一样（这里实现了批量更新效果）
  if (existingCallbackPriority === newCallbackPriority) {
    // 比如多次调用setState的函数
    return
  }
  // 打断之前的任务，执行最新优先级的任务
  if (existingCallbackNode !== null) {
    // 取消之前的任务（并发任务）
    Scheduler_cancelCallback(existingCallbackNode)
  }
  // 新的回调任务
  let newCallbackNode = null
  if (newCallbackPriority === SyncLane) {
    // 先把performSyncWorkOnRoot添回到同步队列中
    scheduleSyncCallback(performSyncWorkOnRoot.bind(null, root)) // 任务添加到更新队列
    // 在微任务阶段执行同步更新任务
    queueMicrotask(flushSyncCallbacks)
    newCallbackNode = null
  } else {
    let schedulerPriorityLevel
    //如果不是同步，就需要调度一个新的任务
    // 1 先将lane优先级转换为事件优先级
    // 2 在将事件优先级转换成Schedule优先级，实现任务的优先级调度执行
    switch (lanesToEventPriority(nextLanes)) {
      case DiscreteEventPriority:
        schedulerPriorityLevel = ImmediateSchedulerPriority
        break
      case ContinuousEventPriority:
        schedulerPriorityLevel = UserBlockingSchedulerPriority
        break
      case DefaultEventPriority:
        schedulerPriorityLevel = NormalSchedulerPriority
        break
      case IdleEventPriority:
        schedulerPriorityLevel = IdleSchedulerPriority
        break
      default:
        schedulerPriorityLevel = NormalSchedulerPriority
        break
    }
    // 开始请求时间片调度任务，并会返回任务对象
    newCallbackNode = scheduleCallback(
      schedulerPriorityLevel,
      performConcurrentWorkOnRoot.bind(null, root)
    )
  }
  // 记住当前在根节点的执行的任务是newCallbackNode
  root.callbackNode = newCallbackNode
  // 记住当前在根节点的执行的优先级
  root.callbackPriority = newCallbackPriority
}

/**
 * 在根上执行同步任务
 * @param {*} root
 * @returns
 */
function performSyncWorkOnRoot(root) {
  const lanes = getNextLanes(root, NoLanes)
  renderRootSync(root, lanes)
  const finishedWork = root.current.alternate
  root.finishedWork = finishedWork
  commitRoot(root)
  // 返回null表示这个任务已经完成了，不用继续调度执行
  return null
}

/**
 * 根据fiber并发构建fiber树，要创建真实的DOM节点，还需要将真实节点插入到容器（首次挂载）
 * @param {*} root
 * @param {*} didTimeout
 * @returns
 */
function performConcurrentWorkOnRoot(root, didTimeout) {
  const originalCallbackNode = root.callbackNode
  const lanes = getNextLanes(root, NoLanes)
  if (lanes === NoLanes) {
    return
  }
  //如果不包含阻塞的车道，并且没有超时，就可以并行渲染,就是启用时间分片
  //所以说默认更新车道是同步的,不能启用时间分片
  // 没有阻塞任务
  const nonIncludesBlockingLane = !includesBlockingLane(root, lanes)
  // 没有过期任务
  const nonIncludesExpiredLane = !includesExpiredLane(root, lanes)
  // 任务执行的时候是没有超时时间片执行的
  const nonTimeout = !didTimeout

  //三个变量都是真，才能进行时间分片，也就是进行并发渲染，也就是可以中断执行
  const shouldTimeSlice = nonIncludesBlockingLane && nonIncludesExpiredLane && nonTimeout
  const exitStatus = shouldTimeSlice
    ? renderRootConcurrent(root, lanes)
    : renderRootSync(root, lanes)
  // 说明已经处理完任务了
  if (exitStatus !== RootInProgress) {
    const finishedWork = root.current.alternate
    root.finishedWork = finishedWork
    // 执行commit操作，将变化渲染到页面上
    commitRoot(root)
  }
  if (root.callbackNode === originalCallbackNode) {
    //把此函数返回，下次接着干
    // 返回任务函数继续执行也就是renderRootConcurrent没有完成fiber的render工作
    return performConcurrentWorkOnRoot.bind(null, root)
  }
  return null
  // 第一次渲染是以同步的方式根节点（目的是为了首屏加载更快）
  // 开始进入提交阶段，处理副作用，也就是修改（挂载、删除、更新属性、移动等操作）真实的dom
  // 生成好fiber树后，需要commitWork进行元素的挂载等副作用操作
}

// 并发的处理fiber构建、更新工作
function renderRootConcurrent(root, lanes) {
  //因为在构建fiber树的过程中，此方法会反复进入，会进入多次
  //只有在第一次进来的时候会创建新的fiber树，或者是有更新任务fiber
  if (workInProgressRoot !== root || workInProgressRootRenderLanes !== lanes) {
    prepareFreshStack(root, lanes)
  }
  // 在当前分配的时间片（5ms）内执行fiber树的构建或者说是渲染（渲染指的是构建fiber树）
  workLoopConcurrent()
  if (workInProgress !== null) {
    return RootInProgress // 還有工作继续返回调度
  }
  return workInProgressRootExitStatus
}

function flushPassiveEffects() {
  if (rootWithPendingPassiveEffects !== null) {
    const root = rootWithPendingPassiveEffects
    commitPassiveUnmountEffects(root.current)
    commitPassiveMountEffects(root, root.current)
  }
}

// 进入commit提交阶段
function commitRoot(root) {
  const previousPriority = getCurrentUpdatePriority()
  try {
    // 进行同步任务时，把lane优先级设置为最高界别的离散事件优先级
    setCurrentUpdatePriority(DiscreteEventPriority)
    commitRootImpl(root)
  } finally {
    setCurrentUpdatePriority(previousPriority)
  }
}

function commitRootImpl(root) {
  // commit阶段，清空任务依赖
  workInProgressRoot = null
  workInProgressRootRenderLanes = NoLanes
  root.callbackNode = null
  root.callbackPriority = NoLane
  const { finishedWork } = root
  // 剩下未完成的更新赛道
  const remainingLanes = mergeLanes(finishedWork.lanes, finishedWork.childLanes)
  // 清空已经完成的lane任务
  // 1 重置root.pendingLanes值 2 重置lane的过期时间
  markRootFinished(root, remainingLanes)
  if (
    (finishedWork.subtreeFlags & Passive) !== NoFlags ||
    (finishedWork.flags & Passive) !== NoFlags
  ) {
    if (!rootDoesHavePassiveEffects) {
      rootDoesHavePassiveEffects = true
      // 调度执行useEffect的副作用
      scheduleCallback(NormalSchedulerPriority, flushPassiveEffects)
    }
  }
  // 判断根fiber的子树有没有副作用
  const subtreeHasEffects = (finishedWork.subtreeFlags & MutationMask) !== NoFlags
  const rootHasEffects = (finishedWork.flags & MutationMask) !== NoFlags

  // 如果自己有副作用或者子节点有副作用，就提交进行dom操作
  if (subtreeHasEffects && !rootHasEffects) {
    commitMutationEffectsOnFiber(finishedWork, root)
    // dom变更完成后，立即执行useLayoutEffect
    commitLayoutEffects(finishedWork, root)
    // 在页面渲染完成后，触发执行effect副作用；
    if (rootDoesHavePassiveEffects) {
      rootWithPendingPassiveEffects = root
      rootDoesHavePassiveEffects = false
    }
  }
  // 等dom变更完成后，就将root的current指针指向最新的fiber树
  root.current = finishedWork
  // 继续完成可能未完成的更新（）
  ensureRootIsScheduled(root, now())
}

// 创建workInProgress
function prepareFreshStack(root, lanes) {
  // 创建一个hostRootFiber，准备开始调度任务
  workInProgress = createWorkInProgress(root.current, null)
  workInProgressRootRenderLanes = lanes // 设置当前渲染优先级
  // 设置workInProgressRoot 的值
  workInProgressRoot = root
  // 将update放入到更新队列中
  finishQueueingConcurrentUpdates()
}

function renderRootSync(root, lanes) {
  // 如果新的根和老的根不一样，或者新的渲染优先级和老的渲染优先级不一样
  if (workInProgressRoot !== root || workInProgressRootRenderLanes !== lanes) {
    prepareFreshStack(root, lanes)
  }
  // workLoopSync会改变workInProgressRootExitStatus的值
  workLoopSync()
  return RootCompleted
}

function workLoopConcurrent() {
  // 如果有下一个要构建的fiber，且时间片没有过期
  while (workInProgress !== null && !shouldYield()) {
    // sleep(5);
    performUnitOfWork(workInProgress)
  }
}

// 同步的执行render阶段的fiber树构建（初次mount和更新）
function workLoopSync() {
  while (workInProgress !== null) {
    performUnitOfWork(workInProgress)
  }
}

// 执行处理一个工作单元（从hostRootFiber开始构建fiber tree）
function performUnitOfWork(unitOfWork) {
  // 获取新的fiber节点对应的老的fiber节点
  const current = unitOfWork.alternate
  // beginWork的作用是：完成当前fiber节点的子fiber链表构建（也就是child/return/sibling关系的构建），并返回第一个子fiber节点
  // next是unitOfWork的第一个子节点child
  const next = beginWork(current, unitOfWork, workInProgressRootRenderLanes)
  unitOfWork.memoizedProps = unitOfWork.pendingProps
  if (next === null) {
    // 如果没有子节点，说明当前fiber已经完成了
    completeUnitOfWork(unitOfWork)
  } else {
    // 如果有子节点，就让子节点成为下一个工作单元
    workInProgress = next
  }
}

function completeUnitOfWork(unitOfWork) {
  let completedWork = unitOfWork
  while (completedWork) {
    const current = completedWork.alternate
    const returnFiber = completedWork.return
    const nextSiblingFiber = completedWork.sibling
    // 执行此fiber的完成工作，如果是原生组件的话，就是创建真实dom节点
    completeWork(current, completedWork)
    // 如果有弟弟，则继续构建弟弟的子fiber链表（继续beginWork）
    if (nextSiblingFiber !== null) {
      workInProgress = nextSiblingFiber
      return
    }
    // 如果没有弟弟，说明该fiber就是父fiber的最后一个子节点
    // 也就是说继续往上递归，完成f父fiber的工作（给父fiber执行completeWork）
    completedWork = returnFiber
    workInProgress = completedWork
  }
  // 执行到这里，说明所有的工作已经完成了
  if (workInProgressRootExitStatus === RootInProgress) {
    workInProgressRootExitStatus = RootCompleted
  }
}

function sleep(time) {
  const timeStamp = new Date().getTime()
  const endTime = timeStamp + time
  while (true) {
    if (new Date().getTime() > endTime) {
      return
    }
  }
}

export function requestUpdateLane(fiber) {
  const updateLane = getCurrentUpdatePriority()
  if (updateLane !== NoLane) {
    return updateLane
  }
  const eventLane = getCurrentEventPriority()
  return eventLane
}

/** 获取当前时间 */
export function requestEventTime() {
  currentEventTime = now()
  return currentEventTime
}

function printFiber(fiber) {
  /*
    fiber.flags &= ~Forked;
    fiber.flags &= ~PlacementDEV;
    fiber.flags &= ~Snapshot;
    fiber.flags &= ~PerformedWork;
    */
  if (fiber.flags !== 0) {
    console.log(
      getFlags(fiber.flags),
      getTag(fiber.tag),
      typeof fiber.type === 'function' ? fiber.type.name : fiber.type,
      fiber.memoizedProps
    )
    if (fiber.deletions) {
      for (let i = 0; i < fiber.deletions.length; i++) {
        const childToDelete = fiber.deletions[i]
        console.log(getTag(childToDelete.tag), childToDelete.type, childToDelete.memoizedProps)
      }
    }
  }
  let child = fiber.child
  while (child) {
    printFiber(child)
    child = child.sibling
  }
}
function getTag(tag) {
  switch (tag) {
    case FunctionComponent:
      return `FunctionComponent`
    case HostRoot:
      return `HostRoot`
    case HostComponent:
      return `HostComponent`
    case HostText:
      return HostText
    default:
      return tag
  }
}
function getFlags(flags) {
  if (flags === (Update | Placement | ChildDeletion)) {
    return `自己移动和子元素有删除`
  }
  if (flags === (ChildDeletion | Update)) {
    return `自己有更新和子元素有删除`
  }
  if (flags === ChildDeletion) {
    return `子元素有删除`
  }
  if (flags === (Placement | Update)) {
    return `移动并更新`
  }
  if (flags === Placement) {
    return `插入`
  }
  if (flags === Update) {
    return `更新`
  }
  return flags
}
