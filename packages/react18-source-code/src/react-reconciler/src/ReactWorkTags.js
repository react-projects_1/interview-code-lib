export const FunctionComponent = 0; // 函数组件
export const ClassComponent = 1; // 类组件
export const IndeterminateComponent = 2; // 未確認的类型
export const HostRoot = 3; // 标识这个fiber是根dom节点-根fiber
export const HostComponent = 5; // 标识这个fiber是元素节点-元素fiber
export const HostText = 6; // 标识这个fiber是文本节点-文本fiber
