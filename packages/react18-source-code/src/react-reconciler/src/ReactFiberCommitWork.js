import { MutationMask, Placement, Update, Passive, LayoutMask, Ref } from './ReactFiberFlags'
import { HostText, HostComponent, HostRoot, FunctionComponent } from './ReactWorkTags'
import {
  appendChild,
  insertBefore,
  commitUpdate,
  removeChild,
} from 'react-dom-bindings/src/client/ReactDOMHostConfig'
import {
  HasEffect as HookHasEffect,
  Passive as HookPassive,
  Layout as HookLayout,
} from './ReactHookEffectTags'

let hostParent = null

/**
 * 提交删除副作用
 * @param {*} root
 * @param {*} returnFiber
 * @param {*} deletedFiber删除的fiber
 */
function commitDeletionEffects(root, returnFiber, deletedFiber) {
  let parent = returnFiber
  findParent: while (parent) {
    switch (parent.tag) {
      case HostComponent:
        hostParent = parent.stateNode
        break findParent
      case HostRoot:
        hostParent = parent.stateNode.containerInfo
        break findParent
      default:
        break
    }
    parent = parent.returnFiber
  }
  commitDeletionEffectsOnFiber(root, returnFiber, deletedFiber)
  hostParent = null
}

function commitDeletionEffectsOnFiber(finishedRoot, nearestMountedAncestor, deletedFiber) {
  switch (deletedFiber.tag) {
    case HostComponent:
    case HostText: {
      // 当要删除一个节点的时候，先递归删除子节点（触发子元素的hook等钩子函数）
      recursivelyTraverseDeletionEffects(finishedRoot, nearestMountedAncestor, deletedFiber)
      // 在删除自己
      if (hostParent) {
        removeChild(hostParent, deletedFiber.stateNode)
      }
    }
  }
}

/**
 * 处理父fiber的子fiber删除逻辑
 * @param {*} finishedRoot
 * @param {*} nearestMountedAncestor
 * @param {*} parent
 */
function recursivelyTraverseDeletionEffects(finishedRoot, nearestMountedAncestor, parent) {
  let child = parent.child
  while (child !== null) {
    commitDeletionEffectsOnFiber(finishedRoot, nearestMountedAncestor, child)
    child = child.sibling
  }
}

/**
 * 递归遍历处理父fiber的子fiber的副作用
 * @param {*} root
 * @param {*} parentFiber
 */
function recursivelyTraverseMutationEffects(root, parentFiber) {
  // 在处理子节点的副作用时，先处理删除子节点的副作用
  // 先把父fiber上该删除的节点都删除
  const deletions = parentFiber.deletions
  if (deletions !== null) {
    for (let i = 0; i < deletions.length; i++) {
      const childToDelete = deletions[i]
      commitDeletionEffects(root, parentFiber, childToDelete)
    }
  }
  // 再去处理剩下的子fiber
  if (parentFiber.subtreeFlags & MutationMask) {
    let child = parentFiber.child
    while (child) {
      commitMutationEffectsOnFiber(child, root)
      child = child.sibling
    }
  }
}

// 处理fiber的副作用（新增、移动节点）
function commitReconciliationEffects(finishedWork) {
  const { flags } = finishedWork
  if (flags & Placement) {
    // Placement操作就是把此fiber的真实节点插入到父fiber的真实dom节点上。
    commitPlacement(finishedWork)
    // 使用完毕后，需要把Placement标识删除
    finishedWork.flags &= ~Placement
  }
}

function isHostParent(fiber) {
  return fiber.tag === HostComponent || fiber.tag === HostRoot
}

function getHostParentFiber(finishedWork) {
  let parent = finishedWork.return
  while (parent) {
    if (isHostParent(parent)) {
      return parent
    }
    parent = parent.return
  }
}

/**
 * 把子节点对应的真实dom插入到父节点的DOM中
 * @param {*} node 要插入的fiber节点
 * @param {*} before 锚点
 * @param {*} parent 父节点真实的dom
 */
function insertOrAppendPlacementNode(node, before, parent) {
  const { tag } = node
  const isHost = tag === HostComponent || tag === HostText
  // 如果是原生dom fiber 直接插入即可
  if (isHost) {
    const { stateNode } = node
    if (before) {
      insertBefore(parent, stateNode, before)
    } else {
      appendChild(parent, stateNode)
    }
  } else {
    // 处理非原生fiber节点的插入
    let child = node.child
    if (child !== null) {
      insertOrAppendPlacementNode(child, before, parent)
      let { sibling } = child
      // 插入其他子节点
      while (sibling !== null) {
        insertOrAppendPlacementNode(sibling, before, parent)
        sibling = sibling.sibling
      }
    }
  }
}

/**
 *  找到要插入的锚点
 * 即要找到可以插到它前面的那个fiber节点对应的真实dom元素
 * @param {} fiber
 * @returns
 */
function getHostSibling(fiber) {
  let node = fiber
  // 递归寻找兄弟节点时元素dom元素的fiber
  siblings: while (true) {
    // 如果没有兄弟节点，就往上找（因为）
    while (node.sibling === null) {
      if (node.return === null || isHostParent(node.return)) {
        // 如果往上找遇到了父fiber是根fiber或者是真实dom的fiber，那说明node就是最后一个孩子
        // 不需要继续找了
        return null
      }
      node = node.return
    }
    node = node.sibling

    // 说明弟弟节点不是真实dom fiber（可能是函数组件），就需要继续往下递归找它的孩子
    while (node.tag !== HostComponent && node.tag !== HostText) {
      if (node.flags & Placement) {
        // 是新增节点，继续找弟弟节点，跳出循环
        continue siblings
      } else {
        node = node.child
      }
    }

    if (node === null) {
      return null
    }
    // 找到不是插入的节点
    // Placement有两种含义，一种是待插入的节点，一种是将要移动（位置不稳定）的节点

    if (node && !(node.flags & Placement)) {
      return node.stateNode
    }
  }
}

/**
 * 把此fiber的真实节点插入到父DOM里面
 * @param {*} finishedWork
 */
function commitPlacement(finishedWork) {
  // 先找到父fiber是原生dom的fiber节点
  const parentFiber = getHostParentFiber(finishedWork)
  // 然后准备将fiber节点插入到父fiber节点对应的dom元素中
  switch (parentFiber.tag) {
    case HostComponent: {
      const parent = parentFiber.stateNode
      // 插入之前，需要找到finishedWork的sibling真实dom节点（因为需要插入到它的签名）
      const before = getHostSibling(finishedWork)
      insertOrAppendPlacementNode(finishedWork, before, parent)
      break
    }
    case HostRoot: {
      const parent = parentFiber.stateNode.containerInfo
      // 插入之前，需要找到finishedWork的sibling真实dom节点（因为需要插入到它的签名）
      const before = getHostSibling(finishedWork)
      insertOrAppendPlacementNode(finishedWork, before, parent)
      break
    }
  }
}

/**
 * 遍历fiber树，执行fiber上的副作用
 * @param {*} finishedWork
 * @param {*} root
 */
export function commitMutationEffectsOnFiber(finishedWork, root) {
  const current = finishedWork.alternate
  const flags = finishedWork.flags
  switch (finishedWork.tag) {
    case FunctionComponent: {
      // 先遍历子节点的副作用，递归处理子fiber的副作用
      recursivelyTraverseMutationEffects(root, finishedWork)
      // 然后再处理自己的副作用
      commitReconciliationEffects(finishedWork)
      if (flags | Update) {
        // 说明有layoutEffect的副作用，需要执行
        commitHookEffectListUnmount(HookHasEffect | HookLayout, finishedWork)
      }
      break
    }
    case HostText:
    case HostRoot: {
      // 先遍历子节点的副作用，递归处理子fiber的副作用
      recursivelyTraverseMutationEffects(root, finishedWork)
      // 然后再处理自己的副作用
      commitReconciliationEffects(finishedWork)
      break
    }
    case HostComponent: {
      // 先遍历子节点的副作用，递归处理子fiber的副作用
      recursivelyTraverseMutationEffects(root, finishedWork)
      // 然后再处理自己的副作用
      commitReconciliationEffects(finishedWork)
      // 处理ref的副作用
      if (flags & Ref) {
        commitAttachRef(finishedWork)
      }
      if (flags & Update) {
        // 处理组件更新逻辑
        const instance = finishedWork.stateNode
        if (instance != null) {
          const newProps = finishedWork.memoizedProps
          const oldProps = current !== null ? current.memoizedProps : newProps
          const type = finishedWork.type
          const updatePayload = finishedWork.updateQueue
          finishedWork.updateQueue = null
          if (updatePayload) {
            commitUpdate(instance, updatePayload, type, oldProps, newProps, finishedWork)
          }
        }
      }
      break
    }
    default: {
      break
    }
  }
}

function commitAttachRef(finishedWork) {
  const ref = finishedWork.ref
  if (ref !== null) {
    const instance = finishedWork.stateNode
    if (typeof ref === 'function') {
      ref(instance)
    } else {
      ref.current = instance
    }
  }
}

/** 执行useEffect的destroy副作用 */
export function commitPassiveUnmountEffects(finishedWork) {
  commitPassiveUnmountOnFiber(finishedWork)
}

function commitPassiveUnmountOnFiber(finishedWork) {
  switch (finishedWork.tag) {
    case FunctionComponent: {
      recursivelyTraversePassiveUnmountEffects(finishedWork)
      if (finishedWork.flags & Passive) {
        commitHookPassiveUnmountEffects(
          finishedWork,
          finishedWork.return,
          HookPassive | HookHasEffect
        )
      }
      break
    }
    default: {
      recursivelyTraversePassiveUnmountEffects(finishedWork)
      break
    }
  }
}
function recursivelyTraversePassiveUnmountEffects(parentFiber) {
  if (parentFiber.subtreeFlags & Passive) {
    let child = parentFiber.child
    while (child !== null) {
      commitPassiveUnmountOnFiber(child)
      child = child.sibling
    }
  }
}
function commitHookPassiveUnmountEffects(finishedWork, nearestMountedAncestor, hookFlags) {
  commitHookEffectListUnmount(hookFlags, finishedWork, nearestMountedAncestor)
}

function commitHookEffectListUnmount(flags, finishedWork) {
  const updateQueue = finishedWork.updateQueue
  const lastEffect = updateQueue !== null ? updateQueue.lastEffect : null
  if (lastEffect !== null) {
    const firstEffect = lastEffect.next
    let effect = firstEffect
    do {
      if ((effect.tag & flags) === flags) {
        const destroy = effect.destroy
        effect.destroy = undefined
        if (destroy !== undefined) {
          destroy()
        }
      }
      effect = effect.next
    } while (effect !== firstEffect)
  }
}

/** 执行useEffect的create副作用 */
export function commitPassiveMountEffects(root, finishedWork) {
  commitPassiveMountOnFiber(root, finishedWork)
}

export function commitPassiveMountOnFiber(finishedRoot, finishedWork) {
  const flags = finishedWork.flags
  switch (finishedWork.tag) {
    case FunctionComponent: {
      recursivelyTraversePassiveMountEffects(finishedRoot, finishedWork)
      if (flags & Passive) {
        // 处理fiber上的effect副作用
        commitHookPassiveMountEffect(finishedWork, HookHasEffect | HookPassive)
      }
      break
    }
    case HostRoot: {
      recursivelyTraversePassiveMountEffects(finishedRoot, finishedWork)
      break
    }
  }
}

function recursivelyTraversePassiveMountEffects(finishedRoot, finishedWork) {
  if (finishedWork.subtreeFlags & Passive) {
    let child = finishedWork.child
    while (child) {
      commitPassiveMountOnFiber(finishedRoot, child)
      child = child.sibling
    }
  }
}

function commitHookPassiveMountEffect(finishedWork, hookFlags) {
  commitHookEffectListMount(hookFlags, finishedWork)
}

// 真正处理effect副作用的函数
function commitHookEffectListMount(hookFlags, finishedWork) {
  // 拿到effect链表执行
  const updateQueue = finishedWork.updateQueue
  const lastEffect = updateQueue !== null ? updateQueue.lastEffect : null
  // 有副作用
  if (lastEffect) {
    let firstEffect = lastEffect.next
    let effect = firstEffect
    do {
      if ((effect.tag & hookFlags) === hookFlags) {
        // 说明有副作用
        // effect.tag的值必须是：HookHasEffect & HookEffect的结果才会执行副作用
        const create = effect.create
        effect.destroy = create()
      }
      effect = effect.next
    } while (effect !== firstEffect)
  }
}

export function commitLayoutEffects(finishedWork, root) {
  const current = finishedWork.alternate
  commitLayoutEffectOnFiber(root, current, finishedWork)
}

function commitLayoutEffectOnFiber(finishedRoot, current, finishedWork) {
  const flags = finishedWork.flags
  switch (finishedWork.tag) {
    case FunctionComponent: {
      recursivelyTraverseLayoutEffects(finishedRoot, finishedWork)
      if (flags & Update) {
        commitHookLayoutEffects(finishedWork, HookLayout | HookHasEffect)
      }
      break
    }
    case HostRoot: {
      recursivelyTraverseLayoutEffects(finishedRoot, finishedWork)
      break
    }
    default:
      break
  }
}
function recursivelyTraverseLayoutEffects(root, parentFiber) {
  if (parentFiber.subtreeFlags & LayoutMask) {
    let child = parentFiber.child
    while (child !== null) {
      const current = child.alternate
      commitLayoutEffectOnFiber(root, current, child)
      child = child.sibling
    }
  }
}
function commitHookLayoutEffects(finishedWork, hookFlags) {
  // 复用effect的逻辑
  commitHookEffectListMount(hookFlags, finishedWork)
}
