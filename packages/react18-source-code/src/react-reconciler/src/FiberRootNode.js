import { initializeUpdateQueue } from './ReactFiberClassUpdateQueue';
import { createHostRootFiber } from './ReactFiber';
import { NoLane, NoLanes, createLaneMap } from './ReactFiberLane';

function FiberRootNode(containerInfo) {
  this.containerInfo = containerInfo;
  // 当前根上待生效的lanes赛道
  this.pendingLanes = NoLanes;
  this.callbackNode = null;
  this.callbackPriority = NoLane;
  // 过期时间 存放每个赛道过期时间（数组）
  this.expirationTimes = createLaneMap();
  // 过期的赛道
  this.expiredLanes = NoLanes;
  this.current = null;
}

export function createFiberRoot(containerInfo) {
  const root = new FiberRootNode(containerInfo);
  const uninitializedFiber = createHostRootFiber();
  root.current = uninitializedFiber;
  // stateNode存储这这个fiber节点对应的真实dom节点
  uninitializedFiber.stateNode = root;
  initializeUpdateQueue(uninitializedFiber);
  return root;
}
