import { allowConcurrentByDefault } from 'shared/ReactFeatureFlags';

// 数值越小，优先级越高SyncLane是优先级最高的赛道
export const TotalLanes = 31;
export const NoLanes = 0b0000000000000000000000000000000;
export const NoLane = 0b0000000000000000000000000000000;
export const SyncLane = 0b0000000000000000000000000000001; // 1
// 输入连续优先级（一般针对连续事件，比如scroll，mousemove等）
export const InputContinuousLane = 0b0000000000000000000000000000100; // 4
// 默認赛道
export const DefaultLane = 0b0000000000000000000000000010000; // 16
// 非空闲优先级
export const NonIdleLanes = 0b0001111111111111111111111111111;
// 空闲优先级
export const IdleLane = 0b0100000000000000000000000000000;

export const NoTimestamp = -1;

/** 获取fiberRootNode节点上优先级最高的那个赛道lane */
export function getNextLanes(root, wipLanes) {
  const pendingLanes = root.pendingLanes;
  if (pendingLanes === NoLanes) {
    return NoLanes;
  }
  // 获取所有的车道中最高优先级的车道
  const nextLanes = getHighestPriorityLanes(pendingLanes);
  // 取优先级最高的那个lanes
  if (wipLanes !== NoLanes && wipLanes !== nextLanes) {
    //新的车道值比渲染中的车道大，说明新的车道优先级更低
    if (nextLanes >= wipLanes) {
      return wipLanes;
    }
  }
  return nextLanes;
}

function getHighestPriorityLanes(lanes) {
  return getHighestPriorityLane(lanes);
}
/** 找到lanes中优先级最高的那个赛道，比如ob00011000 则返回：ob00001000 */
export function getHighestPriorityLane(lanes) {
  return lanes & -lanes;
}
/** 判断当前赛道是否包含非空闲的优先级任务 */
export function includesNonIdleWork(lanes) {
  return (lanes & NonIdleLanes) !== NoLanes;
}

export function includesBlockingLane(root, lanes) {
  // 如果开启了这个标志，那么所有的任务都会是并发任务
  if (allowConcurrentByDefault) {
    return false;
  }
  const SyncDefaultLanes = InputContinuousLane | DefaultLane;
  return (lanes & SyncDefaultLanes) !== NoLanes;
}

/** 判断是否是子集关系 */
export function isSubsetOfLanes(set, subset) {
  return (set & subset) === subset;
}

// 返回lanes中最左边1的索引值
function pickArbitraryLaneIndex(lanes) {
  // 00011000 => 7 - 3 = 4 (00010000的index就是4)
  return TotalLanes - Math.clz32(lanes);
}

// 标记root已经完成更新，需要更新pendingLanes，方便进行下次低优先级的更新处理
export function markRootFinished(root, remainingLanes) {
  // 获取不在生效的lanes(就是已经完成的更新的lanes)
  const noLongerPendingLanes = root.pendingLanes & ~remainingLanes;
  root.pendingLanes = remainingLanes;

  // 重置已经完成更新的赛道lanes的过期时间为NoTimestamp
  let lanes = noLongerPendingLanes;
  const expirationTimes = root.expirationTimes;
  while (lanes > 0) {
    const index = pickArbitraryLaneIndex(lanes);
    const lane = 1 << index;
    expirationTimes[index] = NoTimestamp;
    lanes &= ~lane;
  }
}
export function mergeLanes(a, b) {
  return a | b;
}

// 标记根fiber有待更新的赛道
export function markRootUpdated(root, updateLane) {
  root.pendingLanes |= updateLane;
}

/**
 * 给新加的lane标记过期时间，以及找出哪些lane过期了，累加到root上
 * @param {*} root
 * @param {*} eventTime
 */
export function markStarvedLanesAsExpired(root, currentTime) {
  // 获取当前有更新赛 道
  const pendingLanes = root.pendingLanes; // 待生效的lanes更新
  const expirationTimes = root.expirationTimes; // lanes的过期表
  let lanes = pendingLanes;
  while (lanes > 0) {
    // 找到最左边的第一个lane更新级别的索引
    const index = pickArbitraryLaneIndex(lanes);
    const lane = 1 << index;
    const expirationTime = expirationTimes[index];
    if (expirationTime === NoTimestamp) {
      // 设置过期时间
      expirationTimes[index] = computeExpirationTime(lane, currentTime);
    } else if (expirationTime <= currentTime) {
      // 任务过期了，加入到root的expiredLanes
      root.expiredLanes |= lane;
    }
    // 删除该lane
    lanes &= ~lane;
  }
}
function computeExpirationTime(lane, currentTime) {
  switch (lane) {
    // 250ms后必须执行改任务（实现：将并发任务直接改成同步任务                                  ）
    case SyncLane:
    case InputContinuousLane:
      return currentTime + 250;
    // 默认优先级的任務，5s后必须执行该任务（实现：将并发任务直接改成同步任务                                  ）
    case DefaultLane:
      return currentTime + 5000;
    // 沒有过期时间，可以一直被高优先级任务打断
    case IdleLane:
      return NoTimestamp;
    default:
      return NoTimestamp;
  }
}

export function createLaneMap() {
  const laneMap = [];
  for (let i = 0; i < TotalLanes; i++) {
    laneMap.push(NoTimestamp);
  }
  return laneMap;
}

// lanes中是否包含过期的lane更新
export function includesExpiredLane(root, lanes) {
  return (lanes & root.expiredLanes) !== NoLanes;
}
