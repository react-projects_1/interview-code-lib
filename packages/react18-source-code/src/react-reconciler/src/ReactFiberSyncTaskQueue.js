import {
  getCurrentUpdatePriority,
  setCurrentUpdatePriority,
} from './ReactEventPriorities';

let syncQueue = null;
let isFlushingSyncQueue = false;

/* 
  这是React中执行同步调度的方式，先把任务回调存储在任务队列中
*/
export function scheduleSyncCallback(callback) {
  if (syncQueue === null) {
    syncQueue = [callback];
  } else {
    syncQueue.push(callback);
  }
}

export function flushSyncCallbacks() {
  if (!isFlushingSyncQueue && syncQueue !== null) {
    isFlushingSyncQueue = true;
    const previousPriority = getCurrentUpdatePriority();
    try {
      let isSync = true;
      const queue = syncQueue;
      for (let i = 0; i < queue.length; i++) {
        let callback = queue[i];
        do {
          callback = callback(isSync);
          // 如果还有任务，则继续执行
        } while (callback !== null);
      }
      syncQueue = null;
    } finally {
      setCurrentUpdatePriority(previousPriority);
      isFlushingSyncQueue = false;
    }
  }
  return null;
}
