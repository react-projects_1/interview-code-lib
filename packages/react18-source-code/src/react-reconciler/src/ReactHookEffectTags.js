// 有1说明需要执行这个effect副作用
export const HasEffect = 1;
// 只是标识这个hook有useEffect副作用（不一定要执行，必须还有有HasEffect）
export const Passive = 0b1000;
// 只是标识这个hook有useLayoutEffect副作用（不一定要执行，必须还有有HasEffect）
export const Layout = 0b0100;
