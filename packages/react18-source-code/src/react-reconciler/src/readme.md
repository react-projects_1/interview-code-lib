# beginWork 和 completeWork 的作用

1. beginWork 的作用就是构建当前 fiber 节点的子 fiber 链表（这是通过 children 虚拟节点去创建子 fiber 链表的），一句话概括就是根据虚拟 dom 创建 fiber 节点
2. completeWork 的作用就是去完成这个 fiber 节点的工作，比如是原生元素，则就是创建真实 dom 节点
