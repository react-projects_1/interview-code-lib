import { NoFlags } from './ReactFiberFlags';
import { NoLanes } from './ReactFiberLane';
import {
  HostRoot,
  IndeterminateComponent,
  HostComponent,
  HostText,
} from './ReactWorkTags';

export function FiberNode(tag, pendingProps, key) {
  // 标识这个fiber节点的类型（类组件、函数组件、元素...）
  this.tag = tag;
  this.key = key;
  // 虚拟节点的type值（'div'/Component）
  this.type = null;
  // 存储这fiber节点对应的真实节点
  this.stateNode = null;
  // 标记父fiber
  this.return = null;
  // 指向第一个fiber
  this.child = null;
  // 下一个兄弟fiber
  this.sibling = null;

  // 新的props
  this.pendingProps = pendingProps; //等待生效的属性
  this.memoizedProps = null; //已经生效的属性

  //每个fiber还会有自己的状态，每一种fiber 状态存的类型是不一样的
  //类组件对应的fiber 存的就是类的实例的状态,HostRoot存的就是要渲染的元素
  // 存储的有状态值（state/hook）
  this.memoizedState = null;
  // 更新队列
  this.updateQueue = null;

  // 标识当前fiber节点的副作用（插入、删除、更新、移动）
  this.flags = NoFlags;
  // 标识子节点的副作用
  this.subtreeFlags = NoFlags;
  //替身，轮替 在后面讲DOM-DIFF的时候会用到
  this.alternate = null;

  this.index = 0;
  this.deletions = null;
  this.lanes = NoLanes;
  this.childLanes = NoLanes;
  this.ref = null;
}

function createFiber(tag, pendingProps, key) {
  return new FiberNode(tag, pendingProps, key);
}
export function createHostRootFiber() {
  return createFiber(HostRoot, null, null);
}

/**
 * 基于老fiber和新的属性创建新的fiber
 * 1 workInProgress
 *  1.1 情况1：没有老的fiber，直接创建一个新fiber，并互相通过alternate指向
 *  1.2 有alternate老fiber，直接复用老的alternate即可
 * 复用有两层意思
 * 1 复用老fiber对象
 * 2 复用真实dom节点
 * @param {*} current 老fiber
 * @param {*} pendingProps 新的props属性
 */
export function createWorkInProgress(current, pendingProps) {
  // 能调用这个函数说明current和新fiber是同一个类型的fiber
  let workInProgress = current.alternate;
  // 如果不存在就创建
  if (workInProgress === null) {
    workInProgress = createFiber(current.tag, pendingProps, current.key);
    // 复用真实dom元素
    workInProgress.stateNode = current.stateNode;
    // 构建alternate指向关系
    workInProgress.alternate = current;
    current.alternate = workInProgress;
  } else {
    // 如果有替身，则直接复用，并重置一些属性
    workInProgress.pendingProps = pendingProps;
    // 重置副作用标识
    workInProgress.flags = NoFlags;
    workInProgress.subtreeFlags = NoFlags;
    workInProgress.deletions = null;
  }
  workInProgress.type = current.type;
  workInProgress.child = current.child;
  workInProgress.sibling = current.sibling;
  workInProgress.index = current.index;
  workInProgress.updateQueue = current.updateQueue;
  workInProgress.memoizedProps = current.memoizedProps;
  workInProgress.memoizedState = current.memoizedState;
  workInProgress.ref = current.ref;
  workInProgress.lanes = current.lanes;
  workInProgress.childLanes = current.childLanes;
  return workInProgress;
}

export function createFiberFromElement(element) {
  return createFiberFromTypeAndProps(element.type, element.key, element.props);
}

/**
 * 根据虚拟dom创建一个fiber节点
 * @param {*} type
 * @param {*} key
 * @param {*} pendingProps
 * @returns
 */
function createFiberFromTypeAndProps(type, key, pendingProps) {
  let tag = IndeterminateComponent;
  if (typeof type === 'string') {
    tag = HostComponent;
  }
  const fiber = new FiberNode(tag, pendingProps, key);
  fiber.type = type;
  return fiber;
}

// 创建文本类型的fiber
export function createFiberFromText(content) {
  return createFiber(HostText, content, null);
}
