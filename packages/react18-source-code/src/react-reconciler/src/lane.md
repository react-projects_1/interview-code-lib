# react 里的优先级

1. lane 优先级赛道

```js
// 数值越小，优先级越高SyncLane是优先级最高的赛道
export const TotalLanes = 31;
export const NoLanes = 0b0000000000000000000000000000000;
export const NoLane = 0b0000000000000000000000000000000;
export const SyncLane = 0b0000000000000000000000000000001; // 1
// 输入连续优先级（一般针对连续事件，比如scroll，mousemove等）
export const InputContinuousLane = 0b0000000000000000000000000000100; // 4
// 默認赛道
export const DefaultLane = 0b0000000000000000000000000010000; // 16
// 非空闲优先级
export const NonIdleLanes = 0b0001111111111111111111111111111;
// 空闲优先级
export const IdleLane = 0b0100000000000000000000000000000;
```

2. Schedule 中的 priority 优先级

```javascript
export const NoPriority = 0; // 没有优先级
export const ImmediatePriority = 1; // 立即执行的优先级 立即执行
export const UserBlockingPriority = 2; // 有阻塞用户的优先级任务 被高优先级打断需要等250ms后立即执行
export const NormalPriority = 3; // 正常优先级 被高优先级打断需要等5s后立即执行
export const LowPriority = 4; // 低优先级 被高优先级打断需要等10s后立即执行
export const IdlePriority = 5; // 空闲执行的优先级
```
