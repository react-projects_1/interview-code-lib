export const NoFlags = 0b00000000000000000000000000
/**
 * Placement既有插入也有移动的含义
 * 1 在挂载的时候，Placement就是插入新节点。
 * 2 在dom diff的时候。Placement的副作用就是移动的含义（位置不稳定）
 * 因为在commit阶段查找兄弟
 */
// 表示fiber或者子孙fiber存在需要插入或者移动的HostComponent和HostText
export const Placement = 0b00000000000000000000000010
/**
 * 1 HostComponent属性发生变化
 * 2 HostText内容发生变化
 * 3 FC定义了useLayoutEffect钩子
 */
export const Update = 0b00000000000000000000000100
// 表示有需要删除的子HostComponent和HostText节点
export const ChildDeletion = 0b00000000000000000000001000
// 表示FC中定义了useEffect副作用
export const Passive = 0b00000000000000010000000000
// 表示FC中定义了useLayoutEffect副作用
export const LayoutMask = Update
// 表示fiber有ref属性
export const Ref = 0b00000000000000000100000000
export const MutationMask = Placement | Update | ChildDeletion | Ref
