import {
  HostRoot,
  HostText,
  HostComponent,
  IndeterminateComponent,
  FunctionComponent,
} from './ReactWorkTags';
import {
  processUpdateQueue,
  cloneUpdateQueue,
} from './ReactFiberClassUpdateQueue';
import { reconcileChildFibers, mountChildFibers } from './ReactChildFiber';
import { shouldSetTextContent } from 'react-dom-bindings/src/client/ReactDOMHostConfig';
import logger, { indent } from 'shared/logger';
import { renderWithHooks } from './ReactFiberHooks';
import { NoLanes } from './ReactFiberLane';

/**
 * 根据新的虚拟dom生成新的子fiber链表
 * @param {*} current 老fiber
 * @param {*} workInProgress 新fiber
 * @param {*} nextChildren 新fiber的子虚拟dom数组
 */
function reconcileChildren(current, workInProgress, nextChildren) {
  //如果此fiber没能对应的老fiber,说明此fiber是新创建的，如果这个父fiber是新的创建的，它的儿子们也肯定都是新创建的
  if (current === null) {
    workInProgress.child = mountChildFibers(workInProgress, null, nextChildren);
  } else {
    // 如果有老fiber的话，就做dom-diff 那老fiber的子fiber链表和子虚拟dom比较，进行最小化更新
    workInProgress.child = reconcileChildFibers(
      workInProgress,
      current.child,
      nextChildren
    );
  }
}

// 处理HostRoot上的工作（其实就是将）
function updateHostRoot(current, workInProgress, renderLanes) {
  const nextProps = workInProgress.pendingProps;
  cloneUpdateQueue(current, workInProgress);
  // 执行更新队列的任务（hosRoot的任务就是将虚拟dom放到memoizedState中即memoizedState = { element }）
  processUpdateQueue(workInProgress, nextProps, renderLanes);
  const nextState = workInProgress.memoizedState;
  const nextChildren = nextState.element;

  // 根据新的虚拟dom生成子fiber链表
  reconcileChildren(current, workInProgress, nextChildren);
  return workInProgress.child;
}

/**
 * 构建原生组件的子fiber链表
 * @param {*} current 老fiber
 * @param {*} workInProgress 新fiber
 * @returns
 */
function updateHostComponent(current, workInProgress) {
  const { type } = workInProgress;
  const nextProps = workInProgress.pendingProps;
  let nextChildren = nextProps.children;
  // 优化，如果孩子是唯一的文本节点的话，就不创建fiber节点了，而是在更新属性时，作为文本信息设置为textContent
  const isDirectTextChild = shouldSetTextContent(type, nextProps);
  if (isDirectTextChild) {
    nextChildren = null;
  }
  reconcileChildren(current, workInProgress, nextChildren);
  return workInProgress.child;
}

/**
 * 挂载函数组件
 * @param {*} current
 * @param {*} workInProgress
 * @param {*} Component
 * @param {*} renderLanes
 * @returns
 */
function mountIndeterminateComponent(
  current,
  workInProgress,
  Component,
  renderLanes
) {
  const props = workInProgress.pendingProps;
  const value = renderWithHooks(
    current,
    workInProgress,
    Component,
    props,
    renderLanes
  );
  workInProgress.tag = FunctionComponent;
  reconcileChildren(current, workInProgress, value);
  return workInProgress.child;
}

function updateFunctionComponent(
  current,
  workInProgress,
  Component,
  nextProps,
  renderLanes
) {
  const nextChildren = renderWithHooks(
    current,
    workInProgress,
    Component,
    nextProps,
    renderLanes
  );
  reconcileChildren(current, workInProgress, nextChildren);
  return workInProgress.child;
}

/**
 * 作用是根据新fiber的虚拟dom构建出子fiber链表
 * @param {*} current 老的fiber
 * @param {*} workInProgress 新的fiber
 * @returns
 */
export function beginWork(current, workInProgress, renderLanes) {
  // logger(' '.repeat(indent.number) + 'beginWork', workInProgress);
  // indent.number += 2;
  // 在构建fiber树时，需要重置lanes（下面的流程会重新计算新的lanes，因为老的lanes已经使用过了过期了）
  workInProgress.lanes = NoLanes;
  switch (workInProgress.tag) {
    case IndeterminateComponent: {
      return mountIndeterminateComponent(
        current,
        workInProgress,
        workInProgress.type,
        renderLanes
      );
    }
    case FunctionComponent: {
      const Component = workInProgress.type;
      const nextProps = workInProgress.pendingProps;
      return updateFunctionComponent(
        current,
        workInProgress,
        Component,
        nextProps,
        renderLanes
      );
    }
    case HostRoot:
      return updateHostRoot(current, workInProgress, renderLanes);
    case HostComponent:
      return updateHostComponent(current, workInProgress, renderLanes);
    case HostText:
    default:
      return null;
  }
}
