import { mergeLanes } from './ReactFiberLane';
import { HostRoot } from './ReactWorkTags';

const concurrentQueues = [];
let concurrentQueuesIndex = 0;

/**
 * 返回FiberRootNode，也就是存储根元素的那个FiberRoot
 */
export function markUpdateLaneFromFiberToRoot(fiber) {
  let node = fiber;
  let parent = fiber.return;

  while (parent) {
    node = parent;
    parent = parent.return;
  }
  if (node.tag === HostRoot) {
    const root = node.stateNode;
    return root;
  }
  return null;
}

/** 将更新入队 */
export function enqueueConcurrentHookUpdate(fiber, queue, update, lane) {
  enqueueUpdate(fiber, queue, update, lane);
  // 设置fiber的更新赛道
  fiber.lanes = mergeLanes(fiber.lanes, lane);
  return getRootForUpdatedFiber(fiber);
}

/**
 * 1 这会在useState、useReducer方法被调用时执行update对象入队
 * @param {*} fiber
 *
 * @param {*} queue
 * @param {*} update
 * @param {*} lane
 */
function enqueueUpdate(fiber, queue, update, lane) {
  concurrentQueues[concurrentQueuesIndex++] = fiber;
  concurrentQueues[concurrentQueuesIndex++] = queue;
  concurrentQueues[concurrentQueuesIndex++] = update;
  concurrentQueues[concurrentQueuesIndex++] = lane;
  // 标记lane到fiber节点上
  fiber.lanes = mergeLanes(fiber.lanes, lane);
  const alternate = fiber.alternate;
  if (alternate !== null) {
    alternate.lanes = mergeLanes(alternate.lanes, lane);
  }
}

function getRootForUpdatedFiber(fiber) {
  let node = fiber;
  let parent = node.return;

  while (parent !== null) {
    node = parent;
    parent = parent.return;
  }
  return node.tag === HostRoot ? node.stateNode : null;
}

// 將update挂载到hook的queue链表上
export function finishQueueingConcurrentUpdates() {
  const endIndex = concurrentQueuesIndex;
  concurrentQueuesIndex = 0;
  let i = 0;

  while (i < endIndex) {
    const fiber = concurrentQueues[i++];
    const queue = concurrentQueues[i++];
    const update = concurrentQueues[i++];
    const lane = concurrentQueues[i++];

    if (queue !== null && update !== null) {
      const pending = queue.pending;
      // 构建update的单向循环链表
      if (pending === null) {
        update.next = update;
      } else {
        update.next = pending.next;
        pending.next = update;
      }
      queue.pending = update;
    }
  }
}

// 并发的添加update任务到queue中
export function enqueueConcurrentClassUpdate(fiber, queue, update, lane) {
  enqueueUpdate(fiber, queue, update, lane);
  return getRootForUpdatedFiber(fiber);
}
