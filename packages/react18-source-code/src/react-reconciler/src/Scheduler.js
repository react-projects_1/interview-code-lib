import * as Scheduler from 'scheduler';

export const scheduleCallback = Scheduler.scheduleCallback;
export const NormalPriority = Scheduler.NormalPriority;
export const ImmediatePriority = Scheduler.ImmediatePriority;
export const UserBlockingPriority = Scheduler.UserBlockingPriority;
export const LowPriority = Scheduler.LowPriority;
export const IdlePriority = Scheduler.IdlePriority;
export const shouldYield = Scheduler.shouldYield;
export const Scheduler_cancelCallback = Scheduler.cancelCallback;
export const now = Scheduler.now;
