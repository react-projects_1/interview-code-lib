import {
  NoLane,
  DefaultLane,
  getHighestPriorityLane,
  includesNonIdleWork,
  SyncLane,
  InputContinuousLane,
  IdleLane,
} from './ReactFiberLane';

//默认事件车道
export const DefaultEventPriority = DefaultLane;
//离散事件优先级 click onchange
export const DiscreteEventPriority = SyncLane;
//连续事件的优先级 mousemove
export const ContinuousEventPriority = InputContinuousLane;
//空闲事件优先级
export const IdleEventPriority = IdleLane;

let currentUpdatePriority = NoLane;

export function getCurrentUpdatePriority() {
  return currentUpdatePriority;
}
export function setCurrentUpdatePriority(newPriority) {
  currentUpdatePriority = newPriority;
}
export function isHigherEventPriority(a, b) {
  return a !== 0 && a < b;
}

// 将lane转换成事件的优先级
export function lanesToEventPriority(lanes) {
  //获取最高优先级的lane
  const lane = getHighestPriorityLane(lanes);
  // lane = 1会命中
  if (!isHigherEventPriority(DiscreteEventPriority, lane)) {
    return DiscreteEventPriority; // 1
  }
  // lane = 2 | 4 会命中
  if (!isHigherEventPriority(ContinuousEventPriority, lane)) {
    return ContinuousEventPriority; // 4
  }
  // lane = 8 | 16 会命中
  if (includesNonIdleWork(lane)) {
    return DefaultEventPriority; // 16
  }
  return IdleEventPriority;
}
