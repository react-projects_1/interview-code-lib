import { createFiberRoot } from './FiberRootNode';
import { enqueueUpdate, createUpdate } from './ReactFiberClassUpdateQueue';
import { requestEventTime, scheduleUpdateOnFiber } from './ReactFiberWorkLoop';
import { requestUpdateLane } from './ReactFiberWorkLoop';

export function createContainer(containerInfo) {
  return createFiberRoot(containerInfo);
}

/**
 * 更新容器，把虚拟dom element变成真实DOM插入到container容器中
 * @param {*} element 虚拟DOM
 * @param {*} container DOM容器 FiberRootNode containerInfo div#root
 */
export function updateContainer(element, container) {
  // 拿到当前的根fiber HostRootFiber
  const current = container.current;
  const eventTime = requestEventTime();
  //请求一个更新车道 默认值是16
  const lane = requestUpdateLane(current);
  const update = createUpdate(lane);
  // 将虚拟dom放入update中，等待beginWork阶段处理（就是将虚拟dom变成fiber链表）
  update.payload = { element };
  const root = enqueueUpdate(current, update, lane);
  // 开始调度fiber上的更新任务
  scheduleUpdateOnFiber(root, current, lane, eventTime);
}
