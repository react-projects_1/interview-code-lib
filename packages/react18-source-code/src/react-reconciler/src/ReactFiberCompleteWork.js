import logger, { indent } from 'shared/logger';
import {
  FunctionComponent,
  HostComponent,
  HostRoot,
  HostText,
} from './ReactWorkTags';
import { NoFlags, Ref, Update } from './ReactFiberFlags';
import {
  createTextInstance,
  createInstance,
  appendInitialChild,
  finalizeInitialChildren,
  prepareUpdate,
} from 'react-dom-bindings/src/client/ReactDOMHostConfig';
import { NoLanes, mergeLanes } from './ReactFiberLane';

// 标记有ref副作用
function markRef(workInProgress) {
  workInProgress.flags |= Ref;
}

/**
 * 将子节点真实dom元素添加到父fiber dom元素上
 * @param {*} parent
 * @param {*} workInProgress
 * @returns
 */
function appendAllChildren(parent, workInProgress) {
  let node = workInProgress.child;
  while (node) {
    if (node.tag === HostComponent || node.tag === HostText) {
      // 直接挂载到父节点上
      appendInitialChild(parent, node.stateNode);
      //如果第一个儿子不是一个原生节点，说明它可能是一个函数组件
    } else if (node.child !== null) {
      // 处理非原生元素的fiber节点，需要继续往下查找原生节点
      node = node.child;
      continue;
    }
    // 需要在继续往上处理兄弟节点
    while (node.sibling === null) {
      if (node.return === workInProgress || node.return === null) {
        return;
      }
      //回到父节点（继续找父节点的下一个兄弟节点）
      node = node.return;
    }
    // 处理完后，需要将下一个兄弟节点挂载到父节点上
    node = node.sibling;
  }
}

function markUpdate(workInProgress) {
  workInProgress.flags |= Update;
}

/**
 * 在fiber的完成阶段准备更新dom属性
 * @param {*} current
 * @param {*} workInProgress
 * @param {*} type
 * @param {*} newProps
 */
function updateHostComponent(current, workInProgress, type, newProps) {
  const oldProps = current.memoizedState;
  const instance = workInProgress.stateNode;
  // 比较新老属性，收集属性的差异
  const updatePayload = prepareUpdate(instance, type, oldProps, newProps);
  // 让原生组件的新fiber的更新队列等于[]
  workInProgress.updateQueue = updatePayload;
  if (updatePayload) {
    // 有属性要更新，添加上Update flag标识，然后会在commit提交阶段处理属性的变更到dom上
    markUpdate(workInProgress);
  }
}

/**
 * 完成一个fiber节点
 * @param {*} current 老fiber
 * @param {*} workInProgress 新的构建的fiber
 */
export function completeWork(current, workInProgress) {
  // indent.number -= 2;
  // logger(' '.repeat(indent.number) + 'completeWork', workInProgress);
  const newProps = workInProgress.pendingProps;
  const { type } = workInProgress;
  switch (workInProgress.tag) {
    case HostRoot: {
      bubbleProperties(workInProgress);
      break;
    }
    case FunctionComponent: {
      bubbleProperties(workInProgress);
      break;
    }
    //如果完成的是原生节点的话
    case HostComponent: {
      // 处理更新逻辑
      if (current !== null && workInProgress.stateNode !== null) {
        updateHostComponent(current, workInProgress, type, newProps);
        if (current.ref !== workInProgress.ref) {
          markRef(workInProgress);
        }
      } else {
        // 处理挂载逻辑
        const instance = createInstance(type, newProps, workInProgress);
        // 初次挂载，会把所有子节点挂载到fiber的dom元素上
        // 将所有的子节点添加到completeWork fiber对应的真实dom元素上
        appendAllChildren(instance, workInProgress);
        // 设置dom元素的属性值（style/class等）
        workInProgress.stateNode = instance;
        finalizeInitialChildren(instance, type, newProps);
        if (workInProgress.ref !== null) {
          markRef(workInProgress);
        }
      }
      // 收集子fiber的副作用
      bubbleProperties(workInProgress);
      break;
    }
    // 文本fiber 就是创建文本节点（它会等待父fiber将它插入到自己的下面）
    case HostText: {
      //如果完成的fiber是文本节点，那就创建真实的文本节点
      const newText = newProps;
      const instance = createTextInstance(newText);
      workInProgress.stateNode = instance;
      bubbleProperties(workInProgress);
      break;
    }
  }
}

/**
 * 收集子fiber链表的flags到subtreeFlags上
 * 收集lanes到父节点
 * @param {*} completedWork
 */
function bubbleProperties(completedWork) {
  let newChildLanes = NoLanes;
  let subtreeFlags = NoFlags;
  let child = completedWork.child;
  // 当前节点收集child的副作用到subtreeFlags上
  while (child) {
    newChildLanes = mergeLanes(
      newChildLanes,
      mergeLanes(child.lanes, child.childLanes)
    );
    subtreeFlags |= child.subtreeFlags;
    subtreeFlags |= child.flags;
    child = child.sibling;
  }
  completedWork.childLanes = newChildLanes;
  completedWork.subtreeFlags = subtreeFlags;
}
