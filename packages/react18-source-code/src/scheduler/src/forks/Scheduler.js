import {
  ImmediatePriority,
  UserBlockingPriority,
  NormalPriority,
  LowPriority,
  IdlePriority,
} from '../SchedulerPriorities'
import { push, pop, peek } from '../SchedulerMinHeap'
import { frameYieldMs } from '../SchedulerFeatureFlags'

// 各个优先级任务需要等待的超时时间（超时后回立即执行，不会被高优先级任务打断）
const maxSigned31BitInt = 1073741823
const IMMEDIATE_PRIORITY_TIMEOUT = -1 // 立即执行
const USER_BLOCKING_PRIORITY_TIMEOUT = 250 // 阻塞用户 260ms
const NORMAL_PRIORITY_TIMEOUT = 5000 // 正常任务 5s
const LOW_PRIORITY_TIMEOUT = 10000 // 低优先级任务 10s
const IDLE_PRIORITY_TIMEOUT = maxSigned31BitInt // 空闲时间执行的任务，可以一直被打断
const taskQueue = [] // 存储优先级任务队列
let taskIdCounter = 1 // 任务id 自增
let scheduledHostCallback = null // 当前要执行的回调（这个回调函数内部会取出任务执行）
let startTime = -1 // 记录开启一个时间片的开始时间（方便计算时间片用尽）
let currentTask = null // 当前要执行的任务对象
const channel = new MessageChannel()
const port = channel.port2 // 发送消息的一端
const getCurrentTime = () => performance.now() // 获取当前时间
// 申请来的时间片，会在preformWorkUntilDeadline函数中任务执行，直到用尽时间片（5ms）
channel.port1.onmessage = preformWorkUntilDeadline
function preformWorkUntilDeadline() {
  // 查看是否有任务函数要执行
  // 这里的scheduledHostCallback函数其实就是workLoop函数，在workLoop函数会在
  // 时间片范围内执行任务队列中的高优先级任务
  if (scheduledHostCallback !== null) {
    startTime = getCurrentTime() // 记录时间片的开始时间
    let hasMoreWork = true
    try {
      hasMoreWork = scheduledHostCallback()
    } finally {
      if (hasMoreWork) {
        // 继续申请时间片执行任务（下一轮事件循环）
        schedulePerformWorkUntilDeadline()
      } else {
        scheduledHostCallback = null
      }
    }
  }
}
/**
 * 添加一个优先级任务调度执行
 * @param {*} priorityLevel
 * @param {*} callback
 * @returns
 */
export function scheduleCallback(priorityLevel, callback) {
  const currentTime = getCurrentTime()
  const startTime = currentTime
  let timeout
  switch (priorityLevel) {
    case ImmediatePriority:
      timeout = IMMEDIATE_PRIORITY_TIMEOUT
      break
    case UserBlockingPriority:
      timeout = USER_BLOCKING_PRIORITY_TIMEOUT
      break
    case IdlePriority:
      timeout = IDLE_PRIORITY_TIMEOUT
      break
    case LowPriority:
      timeout = LOW_PRIORITY_TIMEOUT
      break
    case NormalPriority:
    default:
      timeout = NORMAL_PRIORITY_TIMEOUT
      break
  }
  // 任务过期时间
  const expirationTime = startTime + timeout
  const newTask = {
    id: taskIdCounter++,
    callback,
    priorityLevel,
    startTime,
    expirationTime,
    sortIndex: expirationTime,
  }
  push(taskQueue, newTask)
  // 创建好任务并入队后，调度任务执行（也就是会在下个宏任务申请5ms的时间片执行任务队列中的任务）
  requestHostCallback(workLoop)
  return newTask
}

/** 在下一轮事件循环中的宏任务调度任务执行 */
function schedulePerformWorkUntilDeadline() {
  port.postMessage(null)
}

/** 是否应该让出执行权给浏览器（也就是说，如果执行时间超过了一个时间片5ms，就会停止后续的任务执行） */
function shouldYieldToHost() {
  // 已经使用的时间片段时长
  const timeElapsed = getCurrentTime() - startTime
  if (timeElapsed < frameYieldMs) {
    return false
  }
  // 超时了，需要让出执行权
  return true
}

// 执行工作，并返回是否还是工作任务继续执行
function workLoop(initialTime) {
  let currentTime = initialTime
  currentTask = peek(taskQueue)
  while (currentTask !== null) {
    if (currentTask.expirationTime > currentTime && shouldYieldToHost()) {
      // 任务没有过期且时间片用尽了，那么退出任务执行，等待下一轮事件循环执行
      break
    }
    // 如果任务过期了，直接执行，不受时间片的影响。
    const callback = currentTask.callback
    if (typeof callback === 'function') {
      currentTask.callback = null
      // 標記這個任務是否是超时状态下完成
      const didUserCallbackTimeout = currentTask.expirationTime <= currentTime
      const continuationCallback = callback(didUserCallbackTimeout)
      // 任务完成后，更新currentTime
      currentTime = getCurrentTime()
      if (typeof continuationCallback === 'function') {
        // 执行玩一个任务后，又返回一个新的任务，需要继续调度执行任务
        currentTask.callback = continuationCallback
      } else {
        // 任务执行完毕，出队列
        pop(taskQueue)
      }
    } else {
      pop(taskQueue)
    }
    // 拿出下个任务继续执行
    currentTask = peek(taskQueue)
  }
  if (currentTask !== null) {
    return true // 表示还有任务要执行，继续申请下个宏任务的时间片执行任务
  }
  return false
}

function requestHostCallback(callback) {
  scheduledHostCallback = callback
  schedulePerformWorkUntilDeadline()
}

/** 取消一个任务执行 */
function cancelCallback(task) {
  task.callback = null
}

export {
  ImmediatePriority,
  UserBlockingPriority,
  NormalPriority,
  LowPriority,
  IdlePriority,
  shouldYieldToHost as shouldYield,
  cancelCallback,
  getCurrentTime as now,
}
