// schedule的最小实现
const taskQueue = []
let scheduleCallbackFn = null
let startTime = -1
const frameTime = 5
const channel = new MessageChannel()
const port = channel.port1
channel.port2.onmessage = performWorkUntilDeadline

const getCurrentTime = () => performance.now()
const peek = (queue) => queue[0]
const push = (queue, task) => queue.push(task)
const pop = (queue) => queue.shift()
// 开始调度任务的入口函数
function scheduleCallback(callback) {
  const task = {
    callback,
  }
  push(taskQueue, task)
  requestHostCallback(workLoop)
  return task
}
function requestHostCallback(callback) {
  scheduleCallbackFn = callback
  schedulePerformWorkUntilDeadline()
}
function schedulePerformWorkUntilDeadline() {
  port.postMessage(null)
}
function performWorkUntilDeadline() {
  if (scheduleCallbackFn) {
    startTime = getCurrentTime()
    let hasMoreTask = true
    try {
      hasMoreTask = scheduleCallbackFn(startTime)
    } finally {
      if (hasMoreTask) {
        schedulePerformWorkUntilDeadline()
      } else {
        scheduleCallbackFn = null
      }
    }
  }
}

function shouldYieldTime() {
  const useTime = getCurrentTime() - startTime
  if (useTime < frameTime) {
    return false // 没有耗尽时间切片，可以继续执行任务
  }
  return true
}
function workLoop(initialTime) {
  let currentTime = initialTime
  let currentTask = peek(taskQueue)
  while (currentTask) {
    currentTime = getCurrentTime()
    if (shouldYieldTime()) {
      break
    }
    let cb = currentTask.callback
    if (cb) {
      currentTask.callback = null
      // 当前任务是否执行超时了 判断条件是如果当前时间大于过期时间，说明就是超时执行的这个任务
      let didTimeout = false
      const result = cb(didTimeout)
      if (typeof result === 'function') {
        currentTask.callback = result
      } else {
        pop(taskQueue)
      }
    } else {
      pop(taskQueue)
    }
    currentTask = peek(taskQueue)
  }
  if (currentTask) {
    return true
  }
  return false
}
