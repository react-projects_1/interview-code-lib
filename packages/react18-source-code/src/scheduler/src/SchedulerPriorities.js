export const NoPriority = 0; // 没有优先级
export const ImmediatePriority = 1; // 立即执行的优先级 立即执行
export const UserBlockingPriority = 2; // 有阻塞用户的优先级任务 被高优先级打断需要等250ms后立即执行
export const NormalPriority = 3; // 正常优先级 被高优先级打断需要等5s后立即执行
export const LowPriority = 4; // 低优先级 被高优先级打断需要等10s后立即执行
export const IdlePriority = 5; // 空闲执行的优先级
