import {
  setInitialProperties,
  diffProperties,
  updateProperties,
} from './ReactDOMComponent';
import { updateFiberProps, precacheFiberNode } from './ReactDOMComponentTree';
import { getEventPriority } from '../events/ReactDOMEventListener';
import { DefaultEventPriority } from 'react-reconciler/src/ReactEventPriorities';
export function shouldSetTextContent(type, props) {
  return (
    typeof props.children === 'string' || typeof props.children === 'number'
  );
}

export function createTextInstance(text) {
  return document.createTextNode(text);
}

export function createInstance(type, props, workInProgress) {
  const domElement = document.createElement(type);
  // dom节点引用fiber节点
  precacheFiberNode(workInProgress, domElement);
  updateFiberProps(domElement, props);
  return domElement;
}

export const appendInitialChild = (parent, child) => {
  parent.appendChild(child);
};

export const finalizeInitialChildren = (domElement, type, props) => {
  setInitialProperties(domElement, type, props);
};

export const appendChild = (parentInstance, child) => {
  parentInstance.appendChild(child);
};

export const insertBefore = (parentInstance, child, before) => {
  parentInstance.insertBefore(child, before);
};

export function prepareUpdate(domElement, type, oldProps, newProps) {
  return diffProperties(domElement, type, oldProps, newProps);
}

export function commitUpdate(
  domElement,
  updatePayload,
  type,
  oldProps,
  newProps
) {
  // 更新属性
  updateProperties(domElement, updatePayload, type, oldProps, newProps);
  // 更新dom元素上的props属性
  updateFiberProps(domElement, newProps);
}

export function removeChild(parentInstance, child) {
  parentInstance.removeChild(child);
}

// 获取当前事件对应的优先级
export function getCurrentEventPriority() {
  const currentEvent = window.event;
  if (currentEvent === undefined) {
    return DefaultEventPriority;
  }
  return getEventPriority(currentEvent.type);
}
