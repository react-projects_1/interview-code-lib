const randomKey = Math.random().toString(32).slice(2);

export const internalInstanceKey = '__reactFiber$' + randomKey;
export const internalPropsKey = '__reactProps$' + randomKey;

export function getClosestInstanceFromNode(node) {
  return node[internalInstanceKey] || null;
}

export function getFiberCurrentPropsFromNode(node) {
  return node[internalPropsKey] || null;
}

/** 给真实dom节点挂载对应的fiber属性 */
export function precacheFiberNode(hostInst, node) {
  node[internalInstanceKey] = hostInst;
}

/** 给真实dom节点挂载props属性 */
export function updateFiberProps(node, props) {
  node[internalPropsKey] = props;
}
