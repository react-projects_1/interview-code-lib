export function addEventBubbleListener(target, eventName, listener) {
  target.addEventListener(eventName, listener, false);
  return listener;
}

export function addEventCaptureListener(target, eventName, listener) {
  target.addEventListener(eventName, listener, true);
  return listener;
}
