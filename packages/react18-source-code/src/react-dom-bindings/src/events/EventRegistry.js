export const allNativeEvents = new Set();

/**
 * 注册两个阶段的事件
 * @param {*} registrationName react事件名 onClick
 * @param {*} dependencies 原生事件数组[click]
 */
export function registryTwoPhaseEvent(registrationName, dependencies) {
  //注册冒泡事件的对应关系
  registerDirectEvent(registrationName, dependencies);
  //注册捕获事件的对应关系
  registerDirectEvent(registrationName + 'Capture', dependencies);
}

export function registerDirectEvent(registrationName, dependencies) {
  dependencies.forEach((nativeName) => {
    allNativeEvents.add(nativeName);
  });
}
