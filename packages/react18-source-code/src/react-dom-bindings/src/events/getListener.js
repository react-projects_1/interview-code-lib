import { getFiberCurrentPropsFromNode } from '../client/ReactDOMComponentTree';

/**
 * 获取fiber上对应的回调函数
 * @param {*} fiber
 * @param {*} registrationName
 * @returns
 */
export function getListener(fiber, registrationName) {
  const { stateNode } = fiber;
  if (!stateNode) {
    return null;
  }

  const props = getFiberCurrentPropsFromNode(stateNode);
  if (!props) {
    return null;
  }
  return props[registrationName];
}
