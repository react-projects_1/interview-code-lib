import { registryTwoPhaseEvent } from './EventRegistry';

const simpleEventPluginEvents = ['click'];

export const topLevelEventsToReactNames = new Map();

function registerSimpleEvent(domEventName, reactName) {
  //onClick在哪里可以取到
  //workInProgress.pendingProps=React元素或者说虚拟DOM.props
  //const newProps = workInProgress.pendingProps;
  //在源码里 让真实DOM元素   updateFiberProps(domElement, props);
  //const internalPropsKey = "__reactProps$" + randomKey;
  //真实DOM元素[internalPropsKey] = props; props.onClick
  //把原生事件名和处理函数的名字进行映射或者说绑定，click=>onClick
  topLevelEventsToReactNames.set(domEventName, reactName);
  registryTwoPhaseEvent(reactName, [domEventName]);
}

export function registerSimpleEvents() {
  simpleEventPluginEvents.forEach((evenName) => {
    // 原生dom绑定时的事件名
    const domEventName = evenName.toLowerCase();
    // react事件绑定的事件名
    const capitalizedEvent = evenName[0].toUpperCase() + evenName.slice(1);
    registerSimpleEvent(domEventName, `on${capitalizedEvent}`);
  });
}
