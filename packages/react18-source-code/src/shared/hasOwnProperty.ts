const hasOwnProperty = (obj, key) =>
  Object.prototype.hasOwnProperty.call(obj, key);
export default hasOwnProperty;
