function initializeUpdateQueue(fiber) {
  const queue = {
    shared: {
      pending: null,
    },
  };
  fiber.updateQueue = queue;
}

function createUpdate() {
  return { payload: null };
}

/**
 * fiber.updateQueue.shared.pending是一个循环链表
 * pending指向链表尾结点
 * pending.next指向链表头结点
 * @param {*} fiber
 * @param {*} update
 */
function enqueueQueue(fiber, update) {
  const pending = fiber.updateQueue.shared.pending;

  // pending一直指向的都是链表头
  // pending.next指向头结点
  // update每次都是尾结点
  if (pending == null) {
    update.next = update;
  } else {
    // 构建尾结点指向第一个节点关系
    update.next = pending.next;
    // 构建上一个节点指向尾结点关系
    pending.next = update;
  }
  // pending指向尾结点
  fiber.updateQueue.shared.pending = update;
}

function getStateForUpdateQueue(update, prevState) {
  return Object.assign({}, prevState, update.payload);
}

function processUpdateQueue(fiber) {
  const pendingQueue = fiber.updateQueue.shared.pending;
  if (pendingQueue) {
    fiber.updateQueue.shared.pending = null;
    const lastPendingUpdate = pendingQueue;
    const firstPendingUpdate = lastPendingUpdate.next;
    lastPendingUpdate.next = null;

    let newState = fiber.state;
    let update = firstPendingUpdate;
    while (update) {
      newState = getStateForUpdateQueue(update, newState);
      update = update.next;
    }
    fiber.state = newState;
  }
}

const fiber = { state: { id: 1 } };
initializeUpdateQueue(fiber);
const update1 = createUpdate();
update1.payload = { name: 111 };
enqueueQueue(fiber, update1);
const update2 = createUpdate();
update2.payload = { age: 222 };
enqueueQueue(fiber, update2);

processUpdateQueue(fiber);
console.log(fiber);
