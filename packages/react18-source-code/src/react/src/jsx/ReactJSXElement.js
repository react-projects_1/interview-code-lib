import { REACT_ELEMENT_TYPE } from 'shared/ReactSymbols'
import hasOwnProperty from 'shared/hasOwnProperty'

const RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true,
}

function ReactElement(type, key, ref, props) {
  return {
    //这就是React元素，也被称为虚拟DOM
    $$typeof: REACT_ELEMENT_TYPE,
    type, // h1 span
    key, // 唯一标识
    ref, // 后面再讲，是用来获取真实DOM元素
    props, // 属性 children,style,id
  }
}

//React17以前老版的转换函数中key 是放在config里的,第三个参数放children
//React17之后新版的转换函数中key是在第三个参数中的，children是放在config里的
export function jsxDEV(type, config, maybeKey) {
  let key = null
  let ref = null
  let propName
  const props = {}

  if (maybeKey !== undefined) {
    key = maybeKey
  }
  if (config.ref !== undefined) {
    ref = config.ref
  }

  for (propName in config) {
    if (hasOwnProperty(config, propName) && !hasOwnProperty(RESERVED_PROPS, propName)) {
      props[propName] = config[propName]
    }
  }

  return ReactElement(type, key, ref, props)
}
