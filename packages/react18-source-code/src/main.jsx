/* eslint-disable react-refresh/only-export-components */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import * as React from 'react'
import { createRoot } from 'react-dom/client'

let element = (
  <h1
    onClick={() => console.log('父冒泡')}
    onClickCapture={() => {
      console.log('父捕获')
    }}
  >
    <span onClick={() => console.log('子冒泡')} onClickCapture={() => console.log('子捕获')}>
      world
    </span>
  </h1>
)

function FunctionComponent() {
  return element
}
const reducer = (state, action) => {
  if (action.type === 'add') return state + action.payload
  return state
}
function FunctionComponentHooks() {
  const [number, setNumber] = React.useReducer(reducer, 0)
  let attr = {
    id: 1,
    style: null,
  }
  if (number === 6) {
    delete attr.id
    attr.style = { color: 'red' }
  }
  console.log('number', number)
  return (
    <button
      {...attr}
      onClick={() => {
        setNumber({ type: 'add', payload: 1 })
        setNumber({ type: 'add', payload: 2 })
        setNumber({ type: 'add', payload: 3 })
      }}
    >
      {number}
    </button>
  )
}

function FunctionComponentStateHook() {
  const [number, setNumber] = React.useState(0)
  React.useEffect(() => {
    setNumber((n) => n + 1)
    setNumber((n) => n + 2)
    setNumber((n) => n + 100)
  }, [])
  return <button onClick={() => setNumber(number + 1)}>{number}</button>
}

// 单节点dom diff
function FunctionComponentDOMDiff1() {
  const [number, setNumber] = React.useState(0)
  return number === 0 ? (
    <ul key="container" onClick={() => setNumber(number + 1)}>
      <li key="A">A</li>
      <li key="B" id="B">
        B
      </li>
      <li key="C">C</li>
    </ul>
  ) : (
    <ul key="container" onClick={() => setNumber(number + 1)}>
      <li key="B" id="B2">
        B2
      </li>
    </ul>
  )
}
// 多节点dom diff
function FunctionComponentDOMDiffMore() {
  const [number, setNumber] = React.useState(0)
  return number === 0 ? (
    <ul key="container" onClick={() => setNumber(number + 1)}>
      <li key="A">A</li>
      <li key="B">B</li>
      <li key="C">C</li>
      <li key="D">D</li>
      <li key="E">E</li>
      <li key="F">F</li>
    </ul>
  ) : (
    <ul key="container" onClick={() => setNumber(number + 1)}>
      <li key="A">A</li>
      <li key="C">C2</li>
      <li key="E">E2</li>
      <li key="B">B2</li>
      <li key="G">G</li>
      <li key="D">D2</li>
    </ul>
  )
}

function FunctionComponentUseEffect() {
  const [number, setNumber] = React.useState(0)
  React.useEffect(() => {
    console.log('useEffect1')
    return () => {
      console.log('destroy effect1')
    }
  })
  React.useLayoutEffect(() => {
    console.log('useLayoutEffect2')
    return () => {
      console.log('destroy useLayoutEffect2')
    }
  })
  React.useEffect(() => {
    console.log('useEffect3')
    return () => {
      console.log('destroy effect3')
    }
  })
  return <div onClick={() => setNumber((n) => n + 1)}>{number}</div>
}

function ComponentRef() {
  const divRef = React.useRef()
  React.useEffect(() => {
    console.log(111, divRef)
  })
  return <div ref={divRef}>{1111}</div>
}

// 饥饿问题，也就是高优先级任务一直打断低优先级任务的执行
let timer
let count = 0
let bCounter = 0
let cCounter = 0
function ComponentPriorityTask() {
  const [numbers, setNumbers] = React.useState(new Array(100).fill('A'))
  const divRef = React.useRef()
  const updateB = (numbers) => new Array(100).fill(numbers[0] + 'B')
  updateB.id = 'updateB' + bCounter++
  const updateC = (numbers) => new Array(100).fill(numbers[0] + 'C')
  updateC.id = 'updateC' + cCounter++
  React.useEffect(() => {
    timer = setInterval(() => {
      divRef.current.click() // 触发一个高优先级的任务，点击事件时同步类型的任务，优先级是1
      if (count++ === 0) {
        setNumbers(updateB)
      }
      divRef.current.click() // 触发一个高优先级的任务，点击事件时同步类型的任务，优先级是1
      if (count++ > 100) {
        clearInterval(timer)
      }
    })
  }, [])
  // console.log('numbers', numbers);
  return (
    <div ref={divRef} onClick={() => setNumbers(updateC)}>
      {numbers.map((item, index) => (
        <span key={index}>{item}</span>
      ))}
    </div>
  )
} /* 
let counter = 0;
let timer;
let bCounter = 0;
let cCounter = 0;
function FunctionComponent2() {
  const [numbers, setNumbers] = React.useState(new Array(100).fill('A'));
  const divRef = React.useRef();
  const updateB = (numbers) => new Array(100).fill(numbers[0] + 'B');
  updateB.id = 'updateB' + bCounter++;
  const updateC = (numbers) => new Array(100).fill(numbers[0] + 'C');
  updateC.id = 'updateC' + cCounter++;
  React.useEffect(() => {
    timer = setInterval(() => {
      divRef.current.click(); //1
      if (counter++ === 0) {
        setNumbers(updateB); //16
      }
      divRef.current.click(); //1
      if (counter++ > 100) {
        clearInterval(timer);
      }
    });
  }, []);
  return (
    <div
      ref={divRef}
      onClick={() => {
        setNumbers(updateC);
      }}
    >
      {numbers.map((number, index) => (
        <span key={index}>{number}</span>
      ))}
    </div>
  );
} */

function renderRef() {
  const FuncComponent = () => {
    const ref = React.useRef(null)
    React.useEffect(() => {
      console.log(ref)
    })
    return (
      <h1 ref={ref}>
        hello <span style={{ color: 'red' }}>world</span>
      </h1>
    )
  }
  const root = createRoot(document.getElementById('root'))
  root.render(<FuncComponent />)
}
renderRef()
