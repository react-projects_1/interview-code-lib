export const isObject = (val: any) => {
  return typeof val === 'object' && val !== null
}

export const isArray = Array.isArray

export const isString = (val) => typeof val === 'string'

export const isNumber = (val) => typeof val === 'number'

export const isFunction = (val) => typeof val === 'function'

export const isIntegerKey = (key) => isString(key) && parseInt(key, 10) + '' === key

export const hasChanged = (val1, val2) => !Object.is(val1, val2)

const hasOwnProperty = Object.prototype.hasOwnProperty

export const hasOwn = (val: object, key: string | symbol): key is keyof typeof val => {
  if (val == null) {
    return false
  }
  return hasOwnProperty.call(val, key)
}

export const invokeArrayFns = (fns = []) => {
  fns.forEach((fn) => {
    fn()
  })
}

export const enum PatchFlag {
  TEXT = 1 /* 表示是动态文本虚拟节点 */,
  CLASS = 1 << 1, // 动态class
  STYLE = 1 << 2, // 动态style
  PROPS = 1 << 3, // 除了class/style的动态属性
  FULL_PROPS = 1 << 4, // 有key，需要完整diff
  HYDRATE_EVENTS = 1 << 5, // 挂载过事件的
  STABLE_FRAGMENT = 1 << 6, // 稳定序列，子节点顺序不会发生变化
  KEYED_FRAGMENT = 1 << 7, // 子节点有key的fragment
  UNKEYED_FRAGMENT = 1 << 8, // 子节点没有key的fragment
  NEED_PATCH = 1 << 9, // 进行非props比较, ref比较
  DYNAMIC_SLOTS = 1 << 10, // 动态插槽
  DEV_ROOT_FRAGMENT = 1 << 11,
  HOISTED = -1, // 表示静态节点，内容变化，不比较儿子
  BAIL = -2, // 表示diff算法应该结束
}

/* 
  1 这里使用位运算，去设置一个虚拟节点的类型和判断一个虚拟节点的类型
  比如设置一个节点的类型即是元素（ELEMENT），又是数组children
  type = ShapeFlags.ELEMENT | ShapeFlags.ARRAY_CHILDREN
  判断一个节点是否是元素，只需要当前类型与元素类型&即可，不为0表示是元素
  isElement = type & ShapeFlags.ELEMENT;
*/

export const enum ShapeFlags {
  /** html原生元素 */
  ELEMENT = 1,
  FUNCTION_COMPONENT = 1 << 1,
  STATEFUL_COMPONENT = 1 << 2,
  TEXT_CHILDREN = 1 << 3,
  ARRAY_CHILDREN = 1 << 4,
  SLOTS_CHILDREN = 1 << 5,
  TELEPORT_CHILDREN = 1 << 6,
  /** 标记一下，表示在组件卸载时，需要额外处理卸载逻辑 */
  COMPONENT_SHOULD_KEEP_ALIVE = 1 << 8,
  /** 标记一下，表示在组件初次挂载时，有特别的处理逻辑 */
  COMPONENT_KEEP_ALIVE = 1 << 9,
  COMPONENT = ShapeFlags.FUNCTION_COMPONENT | ShapeFlags.STATEFUL_COMPONENT,
}
