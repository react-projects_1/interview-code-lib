import { isObject } from '@vue/shared'
import {
  mutableHandlers,
  shallowReactiveHandlers,
  readonlyHandlers,
  shallowReadonlyHandlers,
} from './baseHandlers'
export const enum ReactiveFlags {
  SKIP = '__v_skip',
  IS_REACTIVE = '__v_isReactive',
  IS_READONLY = '__v_isReadonly',
  IS_SHALLOW = '__v_isShallow',
  RAW = '__v_isRaw',
}

// 从代理对象中拿出原始对象
export function toRaw(value) {
  const val = value && value[ReactiveFlags.RAW]
  // 防止val还是一个代理对象，所以需要递归出来，保证最终取出的是原始对象
  return val ? toRaw(val) : value
}

export function isReadonly(value) {
  return !!(value && value[ReactiveFlags.IS_READONLY])
}

export function isReactive(value) {
  return !!(value && value[ReactiveFlags.IS_REACTIVE])
}

export function isProxy(value) {
  return isReadonly(value) || isReactive(value)
}

function createReactive(target, baseHandlers, proxyMap) {
  if (!isObject(target)) {
    return target
  }

  if (isProxy(target)) {
    return target
  }
  const existProxy = proxyMap.get(target)
  if (existProxy) {
    return existProxy
  }
  const proxy = new Proxy(target, baseHandlers)
  proxyMap.set(target, proxy)
  return proxy
}

export const reactiveMap = new Map()
export const shallowReactiveMap = new Map()
export const readonlyMap = new Map()
export const shallowReadonlyMap = new Map()

// 通过原始对象获取对应的proxy对象
export function getProxy(target, isReadonly, isShallow) {
  return (
    isReadonly
      ? isShallow
        ? shallowReadonlyMap
        : readonlyMap
      : isShallow
      ? shallowReactiveMap
      : reactiveMap
  ).get(target)
}

export function reactive(target) {
  return createReactive(target, mutableHandlers, reactiveMap)
}
export function shallowReactive(target) {
  return createReactive(target, shallowReactiveHandlers, shallowReactiveMap)
}
export function readonly(target) {
  return createReactive(target, readonlyHandlers, readonlyMap)
}
export function shallowReadonly(target) {
  return createReactive(target, shallowReadonlyHandlers, shallowReadonlyMap)
}
