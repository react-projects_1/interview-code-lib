import { isFunction, isObject } from '@vue/shared'
import { ReactiveEffect } from './effect'

function traversal(obj, cache = new Set()) {
  if (!isObject(obj)) {
    return obj
  }
  if (cache.has(obj)) {
    return obj
  }
  cache.add(obj)
  for (const key in obj) {
    traversal(obj[key], cache)
  }
  return obj
}

export function watch(source, cb) {
  let getter
  let cleanup
  let oldValue
  if (isObject(source)) {
    getter = () => traversal(source)
  } else if (isFunction(source)) {
    getter = source
  }

  const collect = (oncleanup) => {
    cleanup = oncleanup
  }
  const job = () => {
    if (cleanup) {
      cleanup()
    }
    const newValue = effect.run()
    cb(newValue, oldValue, collect)
    oldValue = newValue
  }

  const effect = new ReactiveEffect(getter, job)
  // 先依赖收集一次
  oldValue = effect.run()
}
