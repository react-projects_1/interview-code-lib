/* eslint-disable @typescript-eslint/no-this-alias */
let activeEffect: ReactiveEffect | undefined

// 清理effect
function cleanupEffect(effect: ReactiveEffect) {
  if (effect.deps.length) {
    effect.deps.forEach((dep) => {
      dep.delete(effect)
    })
    effect.deps.length = 0
  }
}

export class ReactiveEffect {
  active = true

  parent // 定义一个当前effect的父effect，用于在依赖执行完毕后恢复外层effect继续收集依赖

  // 用来收集当前effect依赖了哪些属性的dep，方便在重新收集的时候先清空上次的dep
  deps: Set<ReactiveEffect>[] = []

  constructor(public fn, public scheduler?: () => void) {}

  run() {
    if (this.active) {
      try {
        this.parent = activeEffect
        activeEffect = this
        // 收集下次依赖时，先清空之前的依赖，防止多余的依赖残留
        cleanupEffect(this)
        return this.fn()
      } finally {
        activeEffect = this.parent
        this.parent = undefined
      }
    } else {
      return this.fn()
    }
  }
  stop() {
    if (this.active) {
      this.active = false
      cleanupEffect(this)
    }
  }
}

export function effect(fn, options?: { scheduler?: () => void }) {
  const _effect = new ReactiveEffect(fn, options?.scheduler)
  _effect.run() // 执行一遍进行依赖收集
  const runner = _effect.run.bind(_effect)
  runner.effect = _effect
  return runner
}

const targetMap = new WeakMap<object, Map<string, Set<ReactiveEffect>>>()

/* 收集依赖 */
export function track(target, type, key) {
  if (activeEffect) {
    let depsMap = targetMap.get(target)
    if (!depsMap) {
      targetMap.set(target, (depsMap = new Map()))
    }
    let deps = depsMap.get(key)
    if (!deps) {
      depsMap.set(key, (deps = new Set()))
    }
    trackEffects(deps)
  }
}

export function trackEffects(deps) {
  if (activeEffect) {
    const shouldTrack = !deps.has(activeEffect)
    if (shouldTrack) {
      deps.add(activeEffect) // 依赖收集
      activeEffect.deps.push(deps) // 同时effect需要记住它被哪些属性依赖了
    }
  }
}

// 触发调用回调函数
export function trigger(target, type, key, value, lodValue) {
  const depMap = targetMap.get(target)
  if (!depMap) {
    return
  }
  const deps = depMap.get(key)
  if (deps) {
    triggerEffects(deps)
  }
}

export function triggerEffects(dep: Set<ReactiveEffect>) {
  // 先拷贝一份，否则在cleanupEffect时会报错
  const effectSet = new Set(dep)
  effectSet.forEach((effect) => {
    // 这是防止在收集依赖的时候又去改变响应式对象的值，导致触发回调执行
    // 如果不做处理，会导致无限递归调用。
    if (effect !== activeEffect) {
      // 这里判断是执行默认的回调函数还是执行用户自定义的调度函数
      if (effect.scheduler) {
        effect.scheduler()
      } else {
        effect.run()
      }
    }
  })
}
