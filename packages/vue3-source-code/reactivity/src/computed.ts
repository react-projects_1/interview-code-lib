import { isFunction } from '@vue/shared'
import { ReactiveEffect, trackEffects, triggerEffects } from './effect'

class ComputedRefImpl {
  _dirty = true
  dep = new Set<ReactiveEffect>()
  effect: ReactiveEffect
  __v_isRef = true
  __v_isReadonly = true
  _value = undefined
  constructor(public getter, public setter) {
    this.effect = new ReactiveEffect(getter, () => {
      if (!this._dirty) {
        this._dirty = true
        triggerEffects(this.dep)
      }
    })
  }

  get value() {
    // 依赖收集
    trackEffects(this.dep)

    if (this._dirty) {
      this._value = this.effect.run()
      this._dirty = false
    }

    return this._value
  }
  set value(val) {
    this.setter(val)
  }
}

export function computed(getterOrOptions) {
  let getter
  let setter

  if (isFunction(getterOrOptions)) {
    getter = getterOrOptions
    setter = () => {
      console.warn('not update')
    }
  } else {
    getter = getterOrOptions.get
    setter = getterOrOptions.set
  }
  return new ComputedRefImpl(getter, setter)
}
