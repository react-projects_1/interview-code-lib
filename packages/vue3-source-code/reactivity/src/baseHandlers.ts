// get/set

import { hasChanged, hasOwn, isArray, isIntegerKey, isObject } from '@vue/shared'
import { ReactiveFlags, getProxy, reactive, readonly } from './reactive'
import { track, trigger } from './effect'
import { TriggerOpTypes } from './operations'

function createSetter() {
  return function (target, key, value, receiver) {
    const oldValue = target[key]
    const result = Reflect.set(target, key, value, receiver)
    const hadKey =
      isArray(target) && isIntegerKey(key) ? Number(key) < target.length : hasOwn(target, key)

    if (!hadKey) {
      // 新增逻辑
      trigger(target, TriggerOpTypes.ADD, key, value, oldValue)
    } else if (hasChanged(oldValue, value)) {
      // 修改逻辑，触发更新
      trigger(target, TriggerOpTypes.SET, key, value, oldValue)
    }

    return result
  }
}

function createGetter(isReadonly?: boolean, isShallow?: boolean) {
  return function (target, key, receiver) {
    // 1 先出来这个代理对象是readonly、shallow类型
    if (key === ReactiveFlags.IS_READONLY) {
      return isReadonly
    } else if (key === ReactiveFlags.IS_REACTIVE) {
      return !isReadonly
    } else if (key === ReactiveFlags.IS_SHALLOW) {
      return isShallow
    } else if (key === ReactiveFlags.RAW && receiver === getProxy(target, isReadonly, isShallow)) {
      // 需要返回原始对象
      return target
    }

    const res = Reflect.get(target, key, receiver)

    if (!isReadonly) {
      // 依赖收集
      track(target, 'get', key)
    }
    if (isShallow) {
      return res
    }

    if (isObject(res)) {
      return isReadonly ? readonly(res) : reactive(res)
    }

    return res
  }
}

const get = createGetter()
const shallowGet = createGetter(false, true)
const readonlyGet = createGetter(true, false)
const shallowReadonlyGet = createGetter(true, true)

const set = createSetter()

export const mutableHandlers = {
  get,
  set,
}
export const shallowReactiveHandlers = {
  get: shallowGet,
  set,
}
export const readonlyHandlers = {
  get: readonlyGet,
  set(target, key, value) {
    console.warn(`Set operation on key "${String(key)}" failed: target is readonly.`, target)
    return true
  },
}
export const shallowReadonlyHandlers = {
  ...readonlyHandlers,
  get: shallowReadonlyGet,
}
