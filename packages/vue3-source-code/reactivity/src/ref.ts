import { isArray, isObject } from '@vue/shared'
import { reactive } from './reactive'
import { ReactiveEffect, trackEffects, triggerEffects } from './effect'

function toReactive(val) {
  return isObject(val) ? reactive(val) : val
}

class RefImpl {
  dep = new Set<ReactiveEffect>()

  _value

  _rawValue
  __v_isRef = true

  constructor(val) {
    this._rawValue = val
    this._value = toReactive(val)
  }

  get value() {
    trackEffects(this.dep)
    return this._value
  }

  set value(val: any) {
    if (this._rawValue !== val) {
      this._rawValue = val
      this._value = toReactive(val)
      // 触发更新
      triggerEffects(this.dep)
    }
  }
}

class ObjectRefImpl {
  __v_isRef = true // 标识是一个ref

  constructor(public obj, public key) {}

  get value() {
    return this.obj[this.key]
  }

  set value(val) {
    this.obj[this.key] = val
  }
}

export function toRefs(obj) {
  if (!isObject(obj)) {
    return obj
  }
  const ret = isArray(obj) ? [] : {}

  for (const key in obj) {
    obj[key] = toRef(obj, key)
  }
  return ret
}

export function toRef(target, key) {
  return new ObjectRefImpl(target, key)
}

export function ref(val) {
  return new RefImpl(val)
}

export function isRef(val) {
  return val && val.__v_isRef === true
}

export function unref(val) {
  return isRef(val) ? val.value : val
}

export function proxyRefs(object) {
  return new Proxy(object, {
    get(target, key, receiver) {
      const val = Reflect.get(target, key, receiver)
      return isRef(val) ? val.value : val
    },
    set(target, key, value, receiver) {
      const val = target[key]

      if (isRef(val)) {
        value.value = value
        return true
      }
      return Reflect.set(target, key, value, receiver)
    },
  })
}
