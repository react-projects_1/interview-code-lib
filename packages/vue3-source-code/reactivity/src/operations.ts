export const enum TriggerOpTypes {
  ADD = 'add',
  SET = 'set',
  DELETE = 'delete',
}

export const enum TrackOpTypes {
  GET = 'get',
  HAS = 'has',
  ITERATE = 'iterate',
}
