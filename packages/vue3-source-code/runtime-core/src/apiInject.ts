import { currentInstance } from './component'
export function provide(key, value) {
  if (!currentInstance) {
    return
  }
  const parentProvides = currentInstance?.parent?.provides
  let provides = currentInstance.provides

  console.log(parentProvides, provides)

  if (parentProvides === provides) {
    // 说明子复用的是父组件的porovides，需要重新拷贝一份
    provides = currentInstance.provides = Object.create(parentProvides)
  }

  provides[key] = value
}

export function inject(key, defaultValue) {
  if (!currentInstance) {
    return
  }

  const provides = currentInstance.provides
  if (key in provides) {
    return provides[key]
  }
  return defaultValue
}
