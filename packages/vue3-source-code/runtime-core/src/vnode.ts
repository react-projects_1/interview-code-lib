import { ShapeFlags, isArray, isObject, isString } from '@vue/shared'
import { isTelePort } from './components/Teleport'

export const Text = Symbol('text')
export const Fragment = Symbol('Fragment')

export function isVnode(node) {
  return node?.__v_isVnode === true
}

export function isSameVnode(node1, node2) {
  return node1.type === node2.type && node1.key === node2.key
}

export function createTextVNode(text) {
  return createVnode(Text, null, text)
}

export interface Vnode {
  type: any
  props?: Record<string, any>
  children?: any
  key?: string

  /** 虚拟节点对应的真实节点 */
  el?: any
  __v_isVnode: true
  /** 标识虚拟节点的类型和children类型 */
  shapeFlag: ShapeFlags
  component?: any
}

export function createVnode(type, props?, children?): Vnode {
  let shapeFlag = isString(type)
    ? ShapeFlags.ELEMENT
    : isTelePort(type)
    ? ShapeFlags.TELEPORT_CHILDREN
    : isObject(type)
    ? ShapeFlags.STATEFUL_COMPONENT
    : 0

  if (children != null) {
    if (isArray(children)) {
      shapeFlag |= ShapeFlags.ARRAY_CHILDREN
    } else if (isObject(children)) {
      shapeFlag |= ShapeFlags.SLOTS_CHILDREN
    } else {
      children = String(children)
      shapeFlag |= ShapeFlags.TEXT_CHILDREN
    }
  }
  return {
    type,
    props,
    children,
    shapeFlag,
    key: props?.key,
    __v_isVnode: true,
    el: null,
    component: null,
  }
}
