const queue = []
let isFlushing = false
const resolvePromise = Promise.resolve()

export function queueJob(job) {
  if (!queue.includes(job)) {
    queue.push(job)
  }

  if (!isFlushing) {
    isFlushing = true
    resolvePromise.then(flushJobs)
  }
}

function flushJobs() {
  let copy = [...queue]
  queue.length = 0
  try {
    for (const job of copy) {
      job()
    }
  } finally {
    copy = null
    isFlushing = false
  }
}
