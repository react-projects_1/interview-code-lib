import { ShapeFlags, hasOwn, isFunction, isObject } from '@vue/shared'
import { initProps } from './componentProps'
import { reactive, proxyRefs } from '@vue/reactivity'

export let currentInstance = null

export const getCurrentInstance = () => currentInstance
export const setCurrentInstance = (instance) => (currentInstance = instance)

export function createComponentInstance(vnode, parent) {
  return {
    parent,
    provides: parent ? parent.provides : {},
    vnode,
    ctx: {} as any,
    data: null, // data函数运行返回的值
    subTree: null, // 组件要挂载的子树
    isMounted: false,
    update: null, // 组件更新函数
    propsOptions: vnode.type.props,

    props: null,
    attrs: null,
    render: null, // 渲染函数，调用会生成最新的subTree子树
    proxy: null, // 代理对象，给render函数运行时绑定的this
    slots: null, // 插槽对象
    setupState: null, // 调用setup函数返回的数据
  }
}

const publicPropsMap = {
  $attrs: (instance) => instance.attrs,
  $slots: (instance) => instance.slots,
}
const publicInstanceProxy = {
  get(target, key) {
    // 需要代理data/props/$attr/$slots的获取
    const { data, props, setupState } = target

    if (data && hasOwn(data, key)) {
      return data[key]
    } else if (props && hasOwn(props, key)) {
      return props[key]
    } else if (setupState && hasOwn(setupState, key)) {
      return setupState[key]
    }
    const getter = publicPropsMap[key]
    if (getter) {
      return getter(target)
    }
  },
  set(target, key, value) {
    // 需要代理data/setupState/props的设置
    const { data, props, setupState } = target
    if (data && hasOwn(data, key)) {
      data[key] = value
    } else if (props && hasOwn(props, key)) {
      console.warn('attempting to mutate prop ' + (key as string))
      return false
    } else if (setupState && hasOwn(setupState, key)) {
      setupState[key] = value
    }
    return true
  },
}

function initSlots(instance, children) {
  if (instance.vnode.shapeFlag & ShapeFlags.SLOTS_CHILDREN) {
    instance.slots = children
  }
}
export function setupComponent(instance) {
  const { type, props, children } = instance.vnode
  // 设置组件的props属性
  initProps(instance, props)
  initSlots(instance, children)

  // 创建代理对象（代理render函数内部访问、设置的data,props,attrs属性）
  instance.proxy = new Proxy(instance, publicInstanceProxy)

  const data = type.data
  if (data) {
    if (!isFunction(data)) {
      console.warn('data must be a function')
    } else {
      instance.data = reactive(data.call(instance.proxy))
    }
  }

  const setup = type.setup

  if (setup) {
    const setupContext = {
      emit: (eventName, ...args) => {
        const name = 'on' + eventName[0].toUpperCase() + eventName.slice(1).toLowerCase()
        const fn = instance.vnode.props[name]
        fn && fn(...args)
      },
      attrs: instance.attrs,
      slots: instance.slots,
    }

    setCurrentInstance(instance)
    const result = setup(instance.props, setupContext)
    setCurrentInstance(null)
    if (isFunction(result)) {
      instance.render = result
    } else if (isObject(result)) {
      // 代理result的ref属性，这样在render函数中就可以直接不用.value调用
      instance.setupState = proxyRefs(result)
    }
  }

  if (!instance.render) {
    instance.render = type.render
  }
}
