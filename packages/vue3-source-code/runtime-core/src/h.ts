/* eslint-disable prefer-rest-params */
/* 
  1 两个参数的情况
  h('div', {style: {}})
  h('div', 'text')
  h('div', h())
  h('div', ['text'] , h())

  2 三个参数的情况
  h('div', {}, 'text')
  h('div', {}, h())
  h('div', {}, h())
  3 三个以上（认为后面的都属于children）
  h('div', {}, h(), ...)

*/

import { isArray, isObject } from '@vue/shared'
import { createVnode, isVnode } from './vnode'

/**
 * 创建虚拟节点的函数
 * 注意：通过h函数转换后，children的类型就变成了数字、文本、数字、文本、虚拟节点数组类型了。
 * 也就是说children可能是数组、文本
 */
export function h(type, propsOrChildren, children) {
  const l = arguments.length
  if (l === 2) {
    if (isObject(propsOrChildren) && !isArray(propsOrChildren)) {
      if (isVnode(propsOrChildren)) {
        return createVnode(type, null, [propsOrChildren])
      } else {
        return createVnode(type, propsOrChildren)
      }
    } else {
      return createVnode(type, null, propsOrChildren)
    }
  } else {
    if (l > 3) {
      children = Array.from(arguments).slice(2)
    } else if (l === 3 && isVnode(children)) {
      children = [children]
    }
    return createVnode(type, propsOrChildren, children)
  }
}
