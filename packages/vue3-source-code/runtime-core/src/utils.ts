// 获取最长递增子序列
export function getSequence(arr) {
  const result = [0]
  // 新增一个回溯数组记录每个元素的前一个节点 [2, 4]表示索引为0的前一个元素是索引为2
  const p = arr.slice()
  let lastIndex
  for (let i = 0; i < arr.length; i++) {
    lastIndex = result.length - 1
    const arrI = arr[i]
    if (arrI !== 0) {
      if (arrI > arr[result[lastIndex]]) {
        result.push(i)
        // 放入元素时，需要记录当前放入元素的前一个元素是谁
        p[i] = result[lastIndex]
      } else {
        let left = 0
        let right = result.length - 1
        while (left < right) {
          const mid = Math.floor((left + right) / 2)
          if (arrI > arr[result[mid]]) {
            left = mid + 1
          } else {
            right = mid
          }
        }
        if (arr[result[left]] > arr[i]) {
          // left 就是第一个比arr[i]小的，需要替换
          if (left > 0) {
            p[i] = result[left - 1]
          }
          result[left] = i
        }
      }
    }
  }

  // 需要重新计算出正确的result
  let i = result.length - 1
  let last = result[i]
  // 从最后一个元素往前回溯它的前一个元素的索引，最终找到正确的索引
  while (i >= 0) {
    result[i] = last
    last = p[last] // 去p中找上一个索引的位置
    i--
  }
  return result
}
/* 
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
0
1
2
2 3
*/

function getSequence2(arr) {
  const p = arr.slice()
  const result = [0]
  let i, j, u, v, c
  const len = arr.length
  for (i = 0; i < len; i++) {
    const arrI = arr[i]
    if (arrI !== 0) {
      j = result[result.length - 1]
      if (arr[j] < arrI) {
        p[i] = j
        result.push(i)
        continue
      }
      u = 0
      v = result.length - 1
      while (u < v) {
        c = (u + v) >> 1
        if (arr[result[c]] < arrI) {
          u = c + 1
        } else {
          v = c
        }
      }
      if (arrI < arr[result[u]]) {
        if (u > 0) {
          p[i] = result[u - 1]
        }
        result[u] = i
      }
    }
  }
  u = result.length
  v = result[u - 1]
  while (u-- > 0) {
    result[u] = v
    v = p[v]
  }
  return result
}
