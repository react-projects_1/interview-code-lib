import { ShapeFlags, invokeArrayFns, isString } from '@vue/shared'
import { Fragment, Text, createTextVNode, isSameVnode } from './vnode'
import { getSequence } from './utils'
import { createComponentInstance, setupComponent } from './component'
import { ReactiveEffect } from '@vue/reactivity'
import { queueJob } from './scheduler'
import { hasPropsChanged, updateProps } from './componentProps'
import { isKeepAlive } from './components/KeepAlive'

export function createRenderer(rendererOptions) {
  const {
    insert: hostInsert,
    remove: hostRemove,
    createElement: hostCreateElement,
    createText: hostCreateText,
    setElementText: hostSetElementText,
    setText: hostSetText,
    nextSibling: hosNextSibling,
    querySelector: hostQuerySelector,
    parentNode: hostParentNode,
    patchProp: hostPatchProp,
  } = rendererOptions

  const processText = (n1, n2, container, anchor) => {
    if (n1 == null) {
      hostInsert((n2.el = hostCreateText(n2.children)), container, anchor)
    } else {
      // 处理更新逻辑
      const el = (n2.el = n1.el)
      if (n1.children !== n2.children) {
        hostSetText(el, n2.children)
      }
    }
  }

  const processFragment = (n1, n2, container, parentComponent) => {
    if (n1 == null) {
      mountChildren(n2.children, container, parentComponent)
    } else {
      patchChildren(n1, n2, container, parentComponent)
    }
  }

  const mountElement = (vnode, container, anchor?, parentComponent?) => {
    const { type, props, children, shapeFlag } = vnode
    const el = (vnode.el = hostCreateElement(type))

    // 处理props
    if (props) {
      for (const prop in props) {
        hostPatchProp(el, prop, null, props[prop])
      }
    }
    // 处理children
    if (shapeFlag & ShapeFlags.TEXT_CHILDREN) {
      hostSetElementText(el, children)
    } else if (shapeFlag & ShapeFlags.ARRAY_CHILDREN) {
      mountChildren(children, el, parentComponent)
    }
    // 挂载到容器上
    hostInsert(el, container, anchor)
  }

  const processElement = (n1, n2, container, anchor?, parentComponent?) => {
    if (n1 == null) {
      mountElement(n2, container, anchor, parentComponent)
    } else {
      patchElement(n1, n2, container, parentComponent)
    }
  }

  const patchElement = (n1, n2, container, parentComponent) => {
    const prevProps = n1.props
    const newProps = n2.props
    // 节点复用
    const el = (n2.el = n1.el)
    // 属性更新
    patchProps(prevProps, newProps, el)
    // 处理children diff
    patchChildren(n1, n2, el, parentComponent)
  }

  /* 
    1 children默认有九种组合（因为children已经被处理成数组、文本、空三种情况了）
    新    旧
    文本 数组 (卸载旧节点，设置新文本)
    文本 文本 （更新设置新文本）
    文本 空   （更新文本，可以和上面那种情况合并成处理）

    数组 数组 （dom diff）
    数组 文本 （卸载文本，挂载新节点）
    数组 空   （挂载新节点，可以和上面的合并成一种情况）

    空 数组  （卸载旧节点）
    空 文本   （卸载文本）
    空 空   （直接不用出来，忽略）

    总结：只需要处理下面六种情况
    文本 数组 （先卸载旧节点，设置新文本）
    文本 文本 （直接更新设置新文本）
    数组 数组 （diff）
    数组 文本 （先卸载文本，在创建新节点）
    空 数组 （直接卸载旧结点）
    空 文本 （卸载文本）
  */
  const patchChildren = (n1, n2, el, parentComponent) => {
    const c1 = n1.children
    const c2 = n2.children
    const shapeFlag = n2.shapeFlag
    const prevShapeFlag = n1.shapeFlag

    if (shapeFlag & ShapeFlags.TEXT_CHILDREN) {
      // 文本 数组
      if (prevShapeFlag & ShapeFlags.ARRAY_CHILDREN) {
        unmountChildren(c1, unmountChildren)
      }
      // 文本 文本
      if (c1 !== c2) {
        hostSetElementText(el, c2)
      }
    } else {
      if (prevShapeFlag & ShapeFlags.ARRAY_CHILDREN) {
        if (shapeFlag & ShapeFlags.ARRAY_CHILDREN) {
          // 数组 数组 vnode diff
          patchKeyedChildren(c1, c2, el, parentComponent)
        } else {
          // 空 数组 unmount
          unmountChildren(c1, unmountChildren)
        }
      } else {
        // 数组或空 文本 卸载文本
        if (prevShapeFlag & ShapeFlags.TEXT_CHILDREN) {
          hostSetElementText(el, '')
        }
        // 数组 文本 挂载新节点
        if (shapeFlag & ShapeFlags.ARRAY_CHILDREN) {
          mountChildren(c2, el, parentComponent)
        }
      }
    }
  }

  const patchKeyedChildren = (c1, c2, container, parentComponent) => {
    // 定义三个指针
    let i = 0 // 表示起始指针
    let e1 = c1.length - 1 // 老children的尾指针
    let e2 = c2.length - 1 // 新children的尾指针

    // sync from start 从前往后比较
    while (i <= e1 && i <= e2) {
      if (isSameVnode(c1[i], c2[i])) {
        patch(c1[i], c2[i], container)
        i++
      } else {
        break
      }
    }
    // sync from end 从后往前比较
    while (i <= e1 && i <= e2) {
      if (isSameVnode(c1[e1], c2[e2])) {
        patch(c1[e1], c2[e2], container)
        e1--
        e2--
      } else {
        break
      }
    }

    // 处理需要新增的节点（经过上面的比较厚）
    if (i > e1) {
      /* 
        之前 abc
        之后是abcdef
        那么经过上面的出来后i = 3 e1 = 2 e2 = 5，此时i > e1 表示可能有新增，
        新增元素的范围是[i, e2]
      */
      // 说明可能有新增的元素
      if (i <= e2) {
        const nextIndex = e2 + 1
        const anchor = nextIndex < c2.length ? c2[nextIndex].el : null
        while (i <= e2) {
          patch(null, c2[i], anchor) // 插入元素
          i++
        }
      }
    } else if (i > e2) {
      // 处理需要删除的节点（经过上面的比较后）
      /* 
        之前 abc
        之后是c
        那么经过上面的出来后i = 0 e1 = 1 e2 = -1，此时i > e2 表示可能有需要删除，
        删除元素的范围是[i, e1]
      */
      if (i <= e1) {
        unmount(c1[i], parentComponent)
        i++
      }
    }

    // 乱序对比（最长递增子序列优化移动过程）

    const s1 = i
    const s2 = i
    // 存储新children的key映射索引的映射表
    const keyToNewIndexMap = new Map() // {[key]: index}
    const toBePatched = e2 - s2 + 1
    // 存储新children c2 中哪些节点已经被复用过了
    const newIndexToOldIndexMap = Array(toBePatched).fill(0)

    for (let i = s2; i <= e2; i++) {
      keyToNewIndexMap.set(c2[i].key, i)
    }

    // 遍历旧children，看哪些可以复用或者删除
    for (let i = s1; i <= e1; i++) {
      const oldVNode = c1[i]
      const newIndex = keyToNewIndexMap.get(oldVNode.key)
      if (newIndex != null) {
        // 可以直接复用
        patch(oldVNode, c2[newIndex], container)
        // 并标记复用的是哪个旧的虚拟节点，在后面的节点移动可以优化（最长递增子序列，减少移动的节点）
        newIndexToOldIndexMap[newIndex - s2] = i + 1
      } else {
        // 没有复用，直接删除
        unmount(oldVNode, parentComponent)
      }
    }

    // 计算出最长递增子序列，然后减少移动次数
    const sequence = getSequence(newIndexToOldIndexMap)
    let lastIndex = sequence.length - 1

    // 上面处理了复用删除逻辑，但是元素的位置需要移动校验对
    for (let i = toBePatched - 1; i >= 0; i--) {
      // 从反向往前插入（因为前面的已经复用过了，肯定有el节点）
      const index = i + s2
      const current = c2[index]
      const anchor = index + 1 < c2.length ? c2[index + 1].el : null
      // 判断是新增还是更新
      if (newIndexToOldIndexMap[i] === 0) {
        // 新增元素，patch创建元素并挂载
        patch(null, current, container, anchor)
      } else {
        // 说明是已经复用过了，只需要移动位置
        if (i === sequence[lastIndex]) {
          // 跳过
          lastIndex--
        } else {
          // 新增元素，patch创建元素并挂载
          hostInsert(current.el, container, anchor)
          // console.log('move')
        }
      }
    }
  }

  const unmountChildren = (children, parentComponent) => {
    for (const child of children) {
      unmount(child, parentComponent)
    }
  }

  const patchProps = (prevProps, newProps, el) => {
    // 1 赋值新属性
    if (newProps) {
      for (const prop in newProps) {
        hostPatchProp(el, prop, prevProps?.[prop], newProps[prop])
      }
    }
    // 删除旧属性
    if (prevProps) {
      for (const prop in prevProps) {
        if (!newProps?.[prop]) {
          hostPatchProp(el, prop, prevProps?.[prop], null)
        }
      }
    }
  }

  const normalize = (children, i) => {
    if (isString(children[i])) {
      children[i] = createTextVNode(children[i])
    }
    return children[i]
  }
  const mountChildren = (children, el, parentComponent) => {
    for (let i = 0; i < children.length; i++) {
      patch(null, normalize(children, i), el, null, parentComponent)
    }
  }

  const processComponent = (n1, n2, container, anchor, parentComponent?) => {
    if (n1 == null) {
      if (n2.shapeFlag & ShapeFlags.COMPONENT_KEEP_ALIVE) {
        parentComponent.ctx.activate(n2, container, anchor)
      } else {
        mountComponent(n2, container, anchor, parentComponent)
      }
    } else {
      updateComponent(n1, n2, container, anchor)
    }
  }
  const mountComponent = (vnode, container, anchor, parentComponent?) => {
    // 1 创建组件实例
    const instance = (vnode.component = createComponentInstance(vnode, parentComponent))

    // 给KeepAlive组件挂载需要的api
    if (isKeepAlive(vnode)) {
      instance.ctx.renderer = {
        createElement: hostCreateElement, // 创建元素用这个方法
        move(vnode, container) {
          // move的vnode肯定是组件
          hostInsert(vnode.component.subTree.el, container)
        },
      }
    }
    // 2 设置组件实例属性
    setupComponent(instance)
    // 3 创建组件的effect（渲染组件内容）
    setupRenderEffect(instance, container, anchor)
  }

  const updateComponentPreRender = (instance, next) => {
    instance.next = null
    instance.vnode = next
    // 更新props值
    updateProps(instance.props, next.props)
    // 合并更新slots的值
    Object.assign(instance.slots, next.children)
  }

  const setupRenderEffect = (instance, container, anchor) => {
    const componentUpdateFn = () => {
      if (instance.isMounted) {
        // 更新逻辑
        const { next, bu, u } = instance

        if (next) {
          // 准备更新前的出来-更新props让子组件可以拿到最新的props
          updateComponentPreRender(instance, next)
        }
        if (bu) {
          invokeArrayFns(bu)
        }

        const subTree = instance.render.call(instance.proxy)
        patch(instance.subTree, subTree, container, anchor, instance)
        instance.subTree = subTree
        if (u) {
          invokeArrayFns(u)
        }
      } else {
        const { bm, m } = instance
        if (bm) {
          invokeArrayFns(bm)
        }
        const subTree = instance.render.call(instance.proxy)
        patch(null, subTree, container, anchor, instance)
        instance.isMounted = true
        instance.subTree = subTree
        if (m) {
          invokeArrayFns(m)
        }
      }
    }
    const effect = new ReactiveEffect(componentUpdateFn, () => {
      queueJob(instance.update)
    })
    const update = (instance.update = effect.run.bind(effect))
    // 手动执行一次，完成初始化渲染组件
    update()
  }

  const shouldUpdateComponent = (n1, n2) => {
    const { props: prevProps, children: prevChildren } = n1
    const { props: nextProps, children: nextChildren } = n2

    if (prevChildren || nextChildren) {
      // 插槽有变化也需要更新
      return true
    }
    if (prevProps === nextProps) return false

    return hasPropsChanged(prevProps, nextProps)
  }

  // 更新组件
  const updateComponent = (n1, n2, container, anchor) => {
    // 组件实例复用
    const instance = (n2.component = n1.component)

    // 检查是否有组件更新
    if (shouldUpdateComponent(n1, n2)) {
      instance.next = n2
      instance.update()
    }
  }

  const patch = (n1, n2, container, anchor = null, parentComponent = null) => {
    if (n1 === n2) return

    // 先提前处理两个虚拟节点不同的情况，直接把之前的卸载，然后挂载新的vnode
    if (n1 && !isSameVnode(n1, n2)) {
      unmount(n1, parentComponent)
      n1 = null
    }

    const { type, shapeFlag } = n2
    // 挂载逻辑
    switch (type) {
      case Text:
        processText(n1, n2, container, anchor)
        break
      case Fragment:
        processFragment(n1, n2, container, parentComponent)
        break
      default:
        if (shapeFlag & ShapeFlags.ELEMENT) {
          processElement(n1, n2, container, anchor, parentComponent)
        }
        if (shapeFlag & ShapeFlags.COMPONENT) {
          processComponent(n1, n2, container, anchor, parentComponent)
        }
        if (shapeFlag & ShapeFlags.TELEPORT_CHILDREN) {
          type.process(n1, n2, container, {
            mountChildren,
            patchChildren,
            move(vnode, el) {
              hostInsert(vnode.component ? vnode.subTree.el : vnode.el, el)
            },
          })
        }
    }
  }

  const unmount = (vnode, parentComponent?) => {
    if (vnode.type === Fragment) {
      return unmountChildren(vnode, parentComponent)
    } else if (vnode.shapeFlag & ShapeFlags.COMPONENT) {
      return unmount(vnode.component.subTree, parentComponent)
    } else if (vnode.shapeFlag & ShapeFlags.COMPONENT_SHOULD_KEEP_ALIVE) {
      return parentComponent.ctx.deactivate(vnode)
    }
    hostRemove(vnode.el)
  }
  // 1 实现元素的挂载和渲染
  const render = (vnode, container) => {
    if (vnode == null) {
      // 卸载逻辑
      if (container._vnode) {
        unmount(container._vnode)
      }
    } else {
      patch(container._vnode, vnode, container)
    }

    container._vnode = vnode
  }
  return {
    render,
  }
}
