import { ShapeFlags } from '@vue/shared'
import { getCurrentInstance } from '../component'
import { onMounted, onUpdated } from '../apiLifecycle'
import { isVnode } from '../../../vue-source-code-demo/packages/runtime-core/src/vnode'

function resetShapeFlag(vnode) {
  let shapeFlag = vnode.shapeFlag

  if (shapeFlag & ShapeFlags.COMPONENT_KEEP_ALIVE) {
    shapeFlag -= ShapeFlags.COMPONENT_KEEP_ALIVE
  } else if (shapeFlag & ShapeFlags.COMPONENT_SHOULD_KEEP_ALIVE) {
    shapeFlag -= ShapeFlags.COMPONENT_SHOULD_KEEP_ALIVE
  }

  vnode.shapeFlag = shapeFlag
}

const KeepAliveImpl = {
  __isKeepAlive: true,
  props: {
    include: {},
    exclude: {},
    max: {},
  },
  setup(props, { slots }) {
    const keys = new Set() // 缓存key，用来判断是否超出max
    // 缓存key对应的虚拟节点（subTree也就是组件的render函数返回的虚拟节点）
    const cache = new Map()
    // 获取当前KeepAlive对应的组件实例（需要挂载方法）
    const instance = getCurrentInstance()
    const { createElement, move } = instance.ctx.renderer
    const storageContainer = createElement('div')
    let pendingCacheKey
    const { max, include, exclude } = props
    let current
    // keepAlive组件卸载时需要调用的方法
    instance.ctx.deactivate = function (vnode) {
      move(vnode, storageContainer)
    }
    // keepAlive组件激活时需要调用的方法
    instance.ctx.activate = function (vnode, container, anchor) {
      move(vnode, container, anchor)
    }

    // 卸载一个组件时调用的方法
    function pruneCacheEntry(key) {
      resetShapeFlag(current)
      cache.delete(key)
      keys.delete(key)
    }

    // keepAlive挂载subTree后需要缓存起来，下次复用
    function cacheSubTree() {
      if (pendingCacheKey) {
        cache.set(pendingCacheKey, instance.subTree)
      }
    }

    // 组件挂载和更新后都需要及时缓存更新subTree，保证下次复用后是最新的subTree
    onMounted(cacheSubTree)
    onUpdated(cacheSubTree)
    return () => {
      const vnode = slots.default()

      if (!isVnode(vnode) || !(vnode.shapeFlag & ShapeFlags.STATEFUL_COMPONENT)) {
        // 不是状态组件，直接跳过不处理
        return vnode
      }

      const Comp = vnode.type
      const key = vnode.key ? vnode.key : Comp
      const name = Comp.name

      if (
        (name && include && !include.split(',').includes(name)) ||
        (exclude && exclude.split(',').includes(name))
      ) {
        return vnode
      }

      const cacheVnode = cache.get(key) // 缓存中查找有没有
      if (cacheVnode) {
        // 复用之前的vnode上的东西
        vnode.component = cacheVnode.component
        // 加上标识，这样在初始化组件时可以走自定义逻辑
        vnode.shapeFlag |= ShapeFlags.COMPONENT_KEEP_ALIVE

        // 更新缓存，将最新的key的位置放在最前面（让它在缓存失效时，优先删除前面的vnode）
        keys.delete(key)
        keys.add(key)
      } else {
        keys.add(key)
        pendingCacheKey = key

        // 处理max缓存超出问题
        if (max && max < keys.size) {
          // 拿到最前面的key去删除（RLU最近最少使用法则）
          pruneCacheEntry(keys.values().next().value)
        }
      }

      // 标记标识，让组件在卸载的时候走deactivate方法
      vnode.shapeFlag |= ShapeFlags.COMPONENT_SHOULD_KEEP_ALIVE
      current = vnode
      return vnode
    }
  },
}

export const isKeepAlive = (vnode) => vnode.type.__isKeepAlive

export const KeepAlive = KeepAliveImpl
