const TeleportImpl = {
  __is_teleport: true,
  process(n1, n2, container, intervals) {
    const { mountChildren, patchChildren, move } = intervals
    if (n1 == null) {
      const target = document.querySelector(n2.props.to)
      if (target) {
        mountChildren(n2.children, target)
      }
    } else {
      patchChildren(n1, n2, container)
      if (n1.props.to !== n2.props.to) {
        // 移动子元素
        const nextTarget = document.querySelector(n2.props.to)
        if (nextTarget) {
          n2.children.forEach((vnode) => move(vnode, nextTarget))
        }
      }
    }
  },
}

export const isTelePort = (Comp) => Comp?.__is_teleport

export const Teleport = TeleportImpl
