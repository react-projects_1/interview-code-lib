import { getCurrentInstance, setCurrentInstance } from './component'

export enum LifecycleHooks {
  BEFORE_MOUNT = 'bm',
  MOUNTED = 'm',
  BEFORE_UPDATE = 'bu',
  UPDATED = 'u',
}

function createHook(type) {
  return (hook, target = getCurrentInstance()) => {
    if (target) {
      const wrapHook = () => {
        setCurrentInstance(target)
        hook()
        setCurrentInstance(null)
      }
      const queue = target[type] || (target[type] = [])
      queue.push(wrapHook)
    }
  }
}

export const onBeforeMount = createHook(LifecycleHooks.BEFORE_MOUNT)
export const onMounted = createHook(LifecycleHooks.MOUNTED)
export const onBeforeUpdate = createHook(LifecycleHooks.BEFORE_UPDATE)
export const onUpdated = createHook(LifecycleHooks.UPDATED)
