import { hasOwn } from '@vue/shared'
import { shallowReactive } from '@vue/reactivity'

export function initProps(instance, rawProps) {
  const props: Record<string, any> = {}
  const attrs: Record<string, any> = {}
  const propsOptions = instance.propsOptions || {}

  if (rawProps) {
    for (const key in rawProps) {
      if (hasOwn(propsOptions, key)) {
        props[key] = rawProps[key]
      } else {
        attrs[key] = rawProps[key]
      }
    }
  }

  // 同时这里将props变成浅的响应式对象
  instance.props = shallowReactive(props)
  instance.attrs = attrs
}

export function hasPropsChanged(prevProps, nextProps) {
  const newKeys = Object.keys(nextProps)
  if (newKeys.length !== Object.keys(prevProps).length) {
    return true
  }

  for (const key in nextProps) {
    if (nextProps[key] !== prevProps[key]) {
      return true
    }
  }

  return false
}

export function updateProps(prevProps = {}, nextProps = {}) {
  // 设置新值到之前上
  for (const key in nextProps) {
    prevProps[key] = nextProps[key]
  }

  for (const key in prevProps) {
    if (!hasOwn(nextProps, key)) {
      delete prevProps[key]
    }
  }
}
