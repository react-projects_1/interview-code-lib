# vue渲染虚拟dom的流程

render(vnode, container)
  unmount
    卸载一个虚拟dom元素
  patch
    根据不同的vnode类型，进行不同的出来
    processText （文本的挂载和更新）
    processElement（元素的挂载和更新）
      mountElement
        1. 处理props
        2. 出来children
        3. 将el挂载到元素上
      updateElement
    processComponent（组件的挂载和更新）
# 元素渲染
# children渲染
# dom diff
# 组件更新流程
# setup实现
# 生命周期实现
# 模版编译
# 代码生成
# provide/inject
# teleport
# 异步组件
# keep-alive