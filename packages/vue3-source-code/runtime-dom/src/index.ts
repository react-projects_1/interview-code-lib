import { createRenderer } from '@vue/runtime-core'
import { nodeOps } from './nodeOps'
import { patchProp } from './patchProp'

const rendererOptions = Object.assign({ patchProp }, nodeOps)

export function render(vnode, container) {
  return createRenderer(rendererOptions).render(vnode, container)
}

export * from '@vue/reactivity'
export * from '@vue/runtime-core'
