import { patchAttr } from './modules/attrs'
import { patchClass } from './modules/class'
import { patchEvent } from './modules/events'
import { patchStyle } from './modules/style'

export function patchProp(el, key, oldValue, value) {
  if (key === 'class') {
    patchClass(el, value)
  } else if (key === 'style') {
    patchStyle(el, oldValue, value)
  } else if (/^on/.test(key)) {
    patchEvent(el, key, value)
  } else {
    patchAttr(el, key, value)
  }
}
