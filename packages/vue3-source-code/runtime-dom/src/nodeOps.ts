export const nodeOps = {
  insert: (child, parent, anchor = null) => parent.insertBefore(child, anchor),

  remove: (el) => {
    el?.parentNode?.removeChild(el)
  },

  createElement: (type) => document.createElement(type),

  createText: (text) => document.createTextNode(text),

  /** 将一个元素的内容直接替换成文本 */
  setElementText: (el, text) => (el.textContent = text),

  setText: (el, text) => (el.nodeValue = text),

  nextSibling: (el) => el.nextSibling,

  querySelector: (selector) => document.querySelector(selector),
  parentNode: (el) => el.parentNode,
}
