function createInvoker(fn) {
  const invoker = (e) => invoker.value()
  invoker.value = fn
  return invoker
}

export function patchEvent(el, eventName, fn) {
  const vei = el._vei || (el._vei = {})
  const currentInvoker = vei[eventName]
  const event = eventName.slice(2).toLowerCase()
  if (currentInvoker) {
    if (fn) {
      // 替换回调函数即可
      currentInvoker.value = fn
    } else {
      // 解除绑定
      el.removeEventListener(event, currentInvoker)
      vei[eventName] = undefined
    }
  } else if (fn) {
    const invoker = createInvoker(fn)
    el.addEventListener(event, invoker)
    vei[eventName] = invoker
  }
}
