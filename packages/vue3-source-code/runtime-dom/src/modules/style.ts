export function patchStyle(el, prevValue, value) {
  // 删除旧属性
  if (prevValue) {
    for (const key in prevValue) {
      if (value?.[key] == null) {
        el.style[key] = null
      }
    }
  }
  // 设置新属性
  if (value) {
    for (const key in value) {
      el.style[key] = value[key]
    }
  }
}
